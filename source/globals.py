import sys
import os

script_dir = os.path.dirname(os.path.abspath(os.path.abspath(__file__)))	# absolute directory of current script's directory
project_dir = os.path.abspath(script_dir + "/..")							# absolute directory of supersonicai 

class globals:
	IMAGE_SIZE = (224, 340, 3)
	IMAGE_SIZE_2D = (224, 340)
	IMAGE_SIZE_3D = IMAGE_SIZE
