# Purpose: Class which generates filepaths used for managing checkpoint files.
# 
# Generates a filename where a new checkpoint will be saved.
# When needed, creates directories where checkpoint files will be saved.
# Gets filenames of existing checkpoint files which will be loaded.

import sys
import os
from datetime import datetime, date

script_dir = os.path.dirname(os.path.abspath(__file__))
project_dir = os.path.abspath(script_dir + "/../..")

os.chdir(script_dir + '/..')

class Checkpoint:
	# --- Static Class Fields ---
	CHECKPOINT_MODE = 1		# for saving/loading NN (.pt) (default)
	PLAYBACK_MODE = 2		# for saving/loading Playback files (.bk2)

	# agent_name - Input should be the name of the agent being used
	# project_dir - The current directory, which should be set at supersonicai
	# datetime - Today's date
	# epoch - The current epoch in training
	# score - The current score

	def __init__(self, agent=None):
		self.agent_name = "" if agent is None else agent.name() # Agent should pass in name
		self.project_dir = project_dir ### was: os.getcwd()
		self.datetime = date.today()
		# How will epoch and score be read? Will it be handled externally and passed in?
		# Or will it have to be evaluated internally
		self.epoch = 0
		self.score = 0
	
	# --- Class Methods ---

	# --------------------------------- SAVING CHECKPOINTS --------------------

	# Concatenates class fields into a filepath to the directory were checkpoints will be saved.
	# Assumes the files parent directory already exists or will be created.
	# ex usage:
	#	cp = Checkpoint()					# creates a checkpoint object
	#	cp.agent_name = "DeepQ"				# sets name of agent
	#	cp.datetime = "2022-10-26"			# ...
	#	cp.epoch = 111						# ...
	#	cp.score = 777						# ...
	#	
	#	path_to_dir = cp.generate_dir()		# determines where checkpoints will be saved
	#	 
	#	print(path_to_dir) 		# "<supersonicai_dir>/results/checkpoints/training/DeepQ"
	#	
	def generate_dir(self, mode=CHECKPOINT_MODE) -> str:
		if (mode == Checkpoint.CHECKPOINT_MODE):
			path = os.path.join(self.project_dir, "results", "checkpoints", "training", self.agent_name)
		elif (mode == Checkpoint.PLAYBACK_MODE):
			path = os.path.join(self.project_dir, "results", "playbacks")

		return path
	
	# Concatenates class fields into a filepath where the checkpoint file. 
	# Assumes directory already exists or will be created.
	# 
	# ex:
	#	cp = Checkpoint()					# creates a checkpoint object
	#	cp.agent_name = "DeepQ"				# sets name of agent
	#	cp.datetime = "2022-10-26"			# ...
	#	cp.epoch = 111						# ...
	#	cp.score = 777						# ...
	#	
	#	filename = cp.generate_path()
	#	
	#	print(filename)			# "<supersonicai_dir>/results/checkpoints/training/DeepQ/2022-10-26_epoch100_score777.pt"
	# 
	def generate_path(self, mode=CHECKPOINT_MODE) -> str:
		return os.path.join(self.generate_dir(mode=mode), str(self.datetime)) + '_' + 'epoch' + str(self.epoch) + '_score' + str(self.score)+ '.pt'
		
	# Creates a directory in the file system where checkpoint files will be stored.
	# See generate_dir()
	# 
	# If the directory exists, this does a no-op.
	#
	# ex:
	#	cp = Checkpoint()					# creates a checkpoint object
	#	cp.agent_name = "DeepQ"				# sets name of agent
	#	cp.datetime = None					# leave blank
	#	cp.epoch = None						# leave blank
	#	cp.score = None						# leave blank
	#	
	#	filename = cp.make_dir()
	#	
	#	print(filename)			# "<supersonicai_dir>/results/checkpoints/training/DeepQ/2022-10-26_epoch100_score777.pt"
	# 
	def make_dir(self, mode=CHECKPOINT_MODE) -> None:
		dir = self.generate_dir(mode=mode)
		# If directory does not exist, create new one, else do nothing
		if not os.path.isdir(dir):
			os.makedirs(dir)

	# --------------------------------- LOADING CHECKPOINTS -------------------

	# gets the latest saved checkpoint file 
	# searches the directory for the checkpoint file with the latest datetime stamp.
	# this might be hard but its possible.
	# 
	# ex:
	#	cp = Checkpoint()					# creates a checkpoint object
	#	cp.agent_name = "DeepQ"				# sets name of agent
	#	cp.datetime = None					# leave blank
	#	cp.epoch = None						# leave blank
	#	cp.score = None						# leave blank
	#	
	#	lastest = cp.get_latest()
	#
	#	print(lastest)		# "<supersonicai_dir>/results/checkpoints/training/DeepQ/2022-10-26_epoch100_score777.pt"
	#
	# If directory is empty, then this should return an empty string
	def get_latest(self) -> str:
		dir = self.generate_dir()	# here is where the checkpoints are saved.
		files = os.listdir(dir)# [ list of all files in checkpoint directory ]
		latest_file = files[0]							# Initialize to the first file
		latest_stamp = files[0].split('_')[0]	# parse datetime stamp somehow
		latest_date = datetime.strptime(latest_stamp, '%Y-%m-%d')

		# --- Search list of files ---
		for file in files:
			# 1.) Somehow get the filename without its path.
			# This is already done

			# 2.) Parse datetime stamp
			date_time = file.split('_')[0]	# somehow parse the date time stamp from filename
			# ex: 
			#	filename = "2022-10-26_epoch100_score777.pt"
			#	datetime = "2022-10-26"

			# 3.) Compare stamps
            # is the datetime later than the latest existing file? if it is, then update latest_file and latest_stamp
			current_date = datetime.strptime(date_time, '%Y-%m-%d')
			# Newer date will have a higher value
			if current_date > latest_date:
				latest_date = current_date
				latest_file = file
		
		return latest_file