from tokenize import String

# Stores names of each level and acts with indexing
class Level:
	__game_name = ""
	__zone_name = ""
	__act_name = ""
		
	def __init__(self, game_name, zone_name, act_name):
		self.__game_name = game_name if game_name != None else "SonicTheHedgehog-Genesis"
		self.__zone_name = zone_name if zone_name != None else "GreenHillZone"
		self.__act_name = act_name if act_name != None else "Act1"

	def game_name(self) -> str:
		return self.__game_name

	def zone_name(self) -> str:
		return self.__zone_name
	
	def act_name(self) -> str:
		return self.__act_name
		
	# Converts level into a string which can be used by gym-retro.
	#	
	# ex: 
	# 	currLevel = interface.Levels.level3
	#	env = retro.make(game=currLevel.game_name, state=currLevel.to_state())
	#	
	def to_state(self) -> str:
		return self.__zone_name + "." + self.__act_name

	def to_string(self) -> str:
		return self.__game_name + ": " + self.__zone_name + "." + self.__act_name

	def print(self) -> None:
		print(self.to_string())
		
