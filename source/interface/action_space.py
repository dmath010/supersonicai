# Represents all possible moves.
# Converts moves to button presses which can be used with env.step()
# 
# ex:
#	# returns an array of 12 ints representing the button presses for this action
#	buttons = ActionSpace.move_right()	
#	
#	env = retro.make(game='SonicTheHedgehog-Genesis', state='LabyrinthZone.Act1')
#	env.step(buttons)	# sends move to game emulator
#
# ActionSpace is treated like a namespace. It is not intended to be instantiated.

class ActionSpace:
	# --------------------------------- FIELDS --------------------------------

	# Index of each possible move.
	# These values are constant and should not be change at runtime.
	STAND_STILL = 0
	RIGHT = 1
	JUMP_RIGHT = 2
	JUMP = 3
	JUMP_LEFT = 4
	LEFT = 5
	CROUCH = 6
	ROLL = CROUCH
	# TODO: Do we need one for spin dash
	
	# look up table which maps action specified by an index [0, 7]
	# to a combination of button presses.
	# *** There is no way to control rolling left or right. ***
	# *** Momentum determines direction of roll. ***
	BUTTONS = [
		# 0  1  2  3  4  5  6  7  8  9 10 11
		# A  B  C     ^  v  <  >              
		[ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ],	# 0 - stand still
		[ 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0 ], # 1 - right
		[ 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0 ], # 2 - jump right
		[ 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 ], # 3 - jump		
		[ 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0 ], # 4 - jump left
		[ 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0 ], # 5 - left
		[ 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0 ], # 6 - crouch/roll
		# TODO: Do we need spin dash?
	]

	# --------------------------------- METHODS -------------------------------

	# returns button presses which coorespond with standing still
	def stand_still() -> list:
		return ActionSpace.BUTTONS[ActionSpace.STAND_STILL]

	# returns button presses which coorespond with moving/running right
	def move_right() -> list:
		return ActionSpace.BUTTONS[ActionSpace.RIGHT]
			
	def jump_right() -> list:
		return ActionSpace.BUTTONS[ActionSpace.JUMP_RIGHT]
			
	def jump() -> list:
		return ActionSpace.BUTTONS[ActionSpace.JUMP]
			
	def jump_left() -> list:
		return ActionSpace.BUTTONS[ActionSpace.JUMP_LEFT]
		
	def move_left() -> list:
		return ActionSpace.BUTTONS[ActionSpace.LEFT]

	def crouch() -> list:
		return ActionSpace.BUTTONS[ActionSpace.CROUCH]

	def roll() -> list:
		return ActionSpace.BUTTONS[ActionSpace.ROLL]

	# returns button presses as a list/array by index.
	# see class ActionSpace fields for aliases for each index
	def move(index) -> list:
		return ActionSpace.BUTTONS[index]

	# Returns the number of possible moves (7 moves)
	def get_n_moves() -> int:
		return len(ActionSpace.BUTTONS)

	# Converts button presses to a string representing the action
	def to_string(buttons) -> str:
		if buttons == ActionSpace.BUTTONS[ActionSpace.STAND_STILL]:
			return 'X'
		if buttons == ActionSpace.BUTTONS[ActionSpace.RIGHT]:
			return '>'
		if buttons == ActionSpace.BUTTONS[ActionSpace.JUMP_RIGHT]:
			return '/'
		if buttons == ActionSpace.BUTTONS[ActionSpace.JUMP]:
			return '|'
		if buttons == ActionSpace.BUTTONS[ActionSpace.JUMP_LEFT]:
			return '\\'
		if buttons == ActionSpace.BUTTONS[ActionSpace.LEFT]:
			return '<'
		if buttons == ActionSpace.BUTTONS[ActionSpace.CROUCH]:
			return 'o'

	def to_string_big(buttons) -> str:
		if buttons == ActionSpace.BUTTONS[ActionSpace.STAND_STILL]:
			return 'XXXXXXX'
		if buttons == ActionSpace.BUTTONS[ActionSpace.RIGHT]:
			return '    -->'
		if buttons == ActionSpace.BUTTONS[ActionSpace.JUMP_RIGHT]:
			return '   |-->'
		if buttons == ActionSpace.BUTTONS[ActionSpace.JUMP]:
			return '   |   '
		if buttons == ActionSpace.BUTTONS[ActionSpace.JUMP_LEFT]:
			return '<--|   '
		if buttons == ActionSpace.BUTTONS[ActionSpace.LEFT]:
			return '<--    '
		if buttons == ActionSpace.BUTTONS[ActionSpace.CROUCH]:
			return 'vvvvvvv'

	# Returns true if buttons is the button press of a jump action
	def is_jump(buttons) -> bool:
		return \
			buttons == ActionSpace.jump() or \
			buttons == ActionSpace.jump_right() or \
			buttons == ActionSpace.jump_left()