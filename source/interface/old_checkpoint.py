import sys
import os
import torch
from datetime import datetime, date

sys.path.append(os.path.abspath('../agents'))
sys.path.append(os.path.abspath('../interface'))
sys.path.append(os.path.abspath('../learning'))

'''
Assumption is that this is run under source/interface, ex: python checkpoint.py
This may need to be changed as this file will be run under play.py
which is located under source/drivers
Agent will call agent.save() at some point
Arguments: agent, index
Returns: path to proper directory

Potential improvement to mypath: make it so that the file can be run anywhere
and still create the proper directory in the same location
because right now, if it is run in supersonicai, it might make a directory elsewhere
'''
def make_checkpoint(agent, index):
    name = agent.name() # Function must be defined to work
    mypath = '../../results/checkpoints/' + name + '/'
    # If agent path does not exist, create it
    if not os.path.isdir(mypath):
        os.makedirs(mypath)
    return mypath + str(index) + '.pt'
    
'''
Load checkpoint from the most recent iteration
Assumption is that the checkpoint file location already exists
Arguments: agent
Returns: most recent checkpoint file and index number in form of list
'''
def load_checkpoint(agent):
    name = agent.name() # Function must be defined to work
    mypath = '../../results/checkpoints/' + name + '/'
    # Count number of checkpoint files currently existing
    num_checks = len(os.listdir(mypath))
    mypath = mypath + str(num_checks) + '.pt'
    return [mypath, num_checks]
