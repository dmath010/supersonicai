import sys
import os
import random

script_dir = os.path.dirname(os.path.abspath(__file__))
project_dir = os.path.abspath(script_dir + "/../..")

from level import *

# Stores names of all levels.
# Stores each level name in an array called levels which can be indexed/iterated easily
# Also has the ability to select a random level.
#
# This class is not meant to be instantiated.
class Levels:
	# ----------------------------- FIELDS --------------------------------

	level1 = Level(game_name="SonicTheHedgehog-Genesis", zone_name="GreenHillZone", act_name="Act1")
	level2 = Level(game_name="SonicTheHedgehog-Genesis", zone_name="GreenHillZone", act_name="Act2")
	level3 = Level(game_name="SonicTheHedgehog-Genesis", zone_name="GreenHillZone", act_name="Act3")
		
	level4 = Level(game_name="SonicTheHedgehog-Genesis", zone_name="MarbleZone", act_name="Act1")
	level5 = Level(game_name="SonicTheHedgehog-Genesis", zone_name="MarbleZone", act_name="Act2")
	level6 = Level(game_name="SonicTheHedgehog-Genesis", zone_name="MarbleZone", act_name="Act3")
		
	level7 = Level(game_name="SonicTheHedgehog-Genesis", zone_name="SpringYardZone", act_name="Act1")
	level8 = Level(game_name="SonicTheHedgehog-Genesis", zone_name="SpringYardZone", act_name="Act2")
	level9 = Level(game_name="SonicTheHedgehog-Genesis", zone_name="SpringYardZone", act_name="Act3")
		
	level10 = Level(game_name="SonicTheHedgehog-Genesis", zone_name="LabyrinthZone", act_name="Act1")
	level11 = Level(game_name="SonicTheHedgehog-Genesis", zone_name="LabyrinthZone", act_name="Act2")
	level12 = Level(game_name="SonicTheHedgehog-Genesis", zone_name="LabyrinthZone", act_name="Act3")
		
	level13 = Level(game_name="SonicTheHedgehog-Genesis", zone_name="StarLightZone", act_name="Act1")
	level14 = Level(game_name="SonicTheHedgehog-Genesis", zone_name="StarLightZone", act_name="Act2")
	level15 = Level(game_name="SonicTheHedgehog-Genesis", zone_name="StarLightZone", act_name="Act3")
		
	level16 = Level(game_name="SonicTheHedgehog-Genesis", zone_name="ScrapBrainZone", act_name="Act1")	
	level17 = Level(game_name="SonicTheHedgehog-Genesis", zone_name="ScrapBrainZone", act_name="Act2")	
	#level18 = Level(game_name="SonicTheHedgehog-Genesis", zone_name="ScrapBrainZone", act_name="Act3")	# TODO: Act 3 does not exist
		
	# TODO: Add level names for other games like Sonic 2
		
	levels = [
		level1, 
		level2, 
		level3, 
		level4, 
		level5, 
		level6, 
		level7, 
		level8, 
		level9, 
		level10,
		level11,
		level12,
		level13,
		level14,
		level15,
		level16,
		level17,
		#level18, # Does not exist
	]

	# ----------------------------- METHODS -------------------------------

	# Selects a level and act at random
	def random() -> Level:
		return random.choice(Levels.levels)

	# Returns a level by index. Index starts at 0.
	def select(lvl) -> Level:
		return Levels.levels[lvl]
