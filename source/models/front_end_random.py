import retro

def main():
    env = retro.make(game='SonicTheHedgehog-Genesis', state='GreenHillZone.Act1', record='.')
    obs = env.reset() # Reset button on emulator
    done = False
    for i in range(2000): 
         # Action space is array of the 12 buttons on genesis controller. 
         # 1 if pressed, 0 if not
        action = env.action_space.sample()
        obs, rew, done, info = env.step(action)
        env.render() #Show emulator

    env.close()

if __name__ == "__main__":
    main()