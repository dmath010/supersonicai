# --- Import packages ---

import sys
import os
import cv2 as cv
import numpy as np

script_dir = os.path.dirname(os.path.abspath(__file__))
project_dir = os.path.abspath(script_dir + "/../..")

sys.path.append(os.path.abspath(project_dir + '/source/agents'))

from actor_critic import *

# --- Which agent should we use? ---
agent = ActorCritic()
print(agent)

print("I am an", agent.name(), "agent")

# --- Create a test image ---
# https://www.delftstack.com/howto/python/opencv-create-image/ 
height = 512
width = 512

img = np.zeros((height, width, 3), np.uint8)
for row in range(0, height):
	for col in range(0, width):
		val = row % (col + 10)
		img[row][col] = (val, val, val)

cv.imshow("Image", img)
cv.waitKey(4000)

#agent.forward(img)
