import os
import sys

script_dir = os.path.dirname(os.path.abspath(__file__))
project_dir = os.path.abspath(script_dir + "/../..")

sys.path.append(os.path.abspath(project_dir + '/source/agents'))

from all_agents import *

agents = [
	ActorCritic(),
	DeepQ(),
	NeatAgent(),
	RandomAgent(),
]

for a in agents:
	print(a.name())

	# a.load(filename)
	
	# buttons = a.decide()

	# buttons.print()
