# Application script which trains an agent to play the game.

from logging import StringTemplateStyle
from pickletools import string1
from tkinter import E
import retro
import numpy as np
import cv2 as cv
import sys
import os
import time
import argparse

script_dir = os.path.dirname(os.path.abspath(__file__))
project_dir = os.path.abspath(script_dir + "/../..")

sys.path.append(os.path.abspath(project_dir + '/source/agents'))
sys.path.append(os.path.abspath(project_dir + '/source/interface'))
sys.path.append(os.path.abspath(project_dir + '/source/learning'))

from levels import Levels
from action_space import *
from all_agents import * 
from all_training import * 
from reward_system import *
from wrappers import *

parser = argparse.ArgumentParser(description='Training Argument')
parser.add_argument('--agent_type', default='dqn', choices=['ac', 'neat', 'dqn'], help='select agent to train (default: %(default)s)') 
parser.add_argument('--level', default=0, type=int, choices=range(0,18), help='select level to train (default: %(default)s)') 
parser.add_argument('--epochs', default='100', type=int, choices=range(0,100000), help='number of epochs to train (default: %(default)s)') 
parser.add_argument('--reward', default='backtrack', choices=['xpos', 'contest', 'complex', 'backtrack'], help='select level to train (default: %(default)s)') 
parser.add_argument('--ckpt', default=None, type=str, help='select starting ckpt, MUST USE RELATIVE PATH (default: None)') 
parser.add_argument('--render', action='store_true', help='renders env during training')
parser.add_argument('--save_rate', default='10', type=int, choices=range(0,1000), help='number of epochs to train (default: %(default)s)')
parser.add_argument('--skip', default='0.25', type=float, choices=np.arange(0,1, .01), help='probability of skipping frames (0-1) (default: %(default)s)')
parser.add_argument('--seg', default=None, type=str, help='select segmentation model, MUST USE RELATIVE PATH (default: None)') 

#parser.add_argument('--plot', action='store_true', help='plot metrics during training')

args = parser.parse_args()

def main():
	print("=== Train Agent ===")
	
	# --- 0.) Initialize objects ---
	random.seed(time.time())	# TODO: Use a time based seed instead

	# ---Agent----

	#----Agent from Ckpt---#
	if args.ckpt is not None:
		if args.agent_type == 'ac':
			agent = ActorCritic(args.ckpt)
		elif args.agent_type == 'random':
			agent = RandomAgent(args.ckpt)
		elif args.agent_type == 'neat':
			agent = NeatAgent(args.ckpt)
		else:
			agent = DeepQ(args.ckpt, args.seg)
		print(f"Agent:{agent.name()} from ckpt {args.ckpt}")
		
	else:
		# --- Agent from Scratch ---
		if args.agent_type == 'ac':
			agent = ActorCritic()
		elif args.agent_type == 'random':
			agent = RandomAgent()
		elif args.agent_type == 'neat':
			agent = NeatAgent()
		else:
			agent = DeepQ(None, args.seg)
		print(f"Agent: {agent.name()} from scratch")
		
	# --- Level ---
	if args.level == 0:
		level = Levels.random()
	else:
		level = Levels.select(args.level - 1)
	print("Level:", level.game_name(), level.to_state())

	# --- Reward System ---

	reward_system = None
	if args.reward == 'xpos':
		reward_system = RewardSystem.XPos()
	elif args.reward == 'contest':
		reward_system = RewardSystem.Contest()
	elif args.reward == 'complex':
		reward_system = RewardSystem.Complex()
	elif args.reward == 'backtrack':
		reward_system = RewardSystem.Backtracking()
	else:
		reward_system = RewardSystem.Complex()
	print("Reward System:", reward_system.to_string())

	# --- Game Environment ---

	env = retro.make(game=level.game_name(), state=level.to_state())
	if args.skip > 0:
		env = StochasticFrameSkip(env,4,args.skip)

	ob = env.reset()

	# --- Epochs ---
	print (f'Training {agent.name()} for {args.epochs} epochs')

	agent.train(env, args.epochs, reward_system=reward_system, render=args.render, ckpt=None, save_rate=args.save_rate)

	# --- 4.) Load its trained data (or initialize untrained model) ---
	# agent.load(filename) 
	# #filename = script_dir + "/../results/checkpoints/DeepQ/" + agent.name() + ".pth"
	# filename = "deepq.pt"
	# print("Saving nn to", filename)
	# agent.save(filename)	# TODO: Carefull not to overwrite files.
		
	# Repeat until game over
	#counter = 0
	#while not done:
		# --- .) Train for 1 Epoch ---
		# --- .) Save Progress ---
		# --- .) 

# Call main
main()