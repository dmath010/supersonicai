# Application script which plays game 
# Does not train or modify the agent in anyway.
# Selects an agent which will play the game along with a parameters file if available.
# Selects a zone of the game to play.
# Renders gameplay to the screen.
# See command line arguments in main()

# Original image: 240x???
# NN image:
# NN output: 7or8 nodes 

import retro
import numpy as np
import cv2 as cv
import sys
import os
import time
import argparse

script_dir = os.path.dirname(os.path.abspath(os.path.abspath(__file__)))	# absolute directory of current script's directory
project_dir = os.path.abspath(script_dir + "/../..")						# absolute directory of supersonicai 

sys.path.append(os.path.abspath(project_dir + '/source/agents'))	# add agents directory
sys.path.append(os.path.abspath(project_dir + '/source/interface'))	# add interface directory
sys.path.append(os.path.abspath(project_dir + '/source/learning'))	# add learning directory
sys.path.append(os.path.abspath(project_dir + '/source/datasets'))	# add learning directory

from levels import *
from action_space import *
from all_agents import * 
from checkpoint import *
from image_processing import * 
from image_tuple import *

# Command line args:
# --agent <agent>				selects the agent which will play the game (default is Neat)
#
# --zone <zone>					selects the zone to play (default is "GreenHillZone")
#								random means select a zone at random
# 
# --act <act>					selects the act of the zone to play (default is "Act1")
#								random means select an act at random
#
# --game <game>					selects the game to play (default is "SonicTheHedgehod-Genesis")
#								random means select a game at random
#
# --list						lists all available options and agents, zones, acts, and games
#
# --input <file>				file containing weights and shape of the agent. If no file is selected, then
#								either a default input file is used or weights will be initialized randomly.
#
# --move-sequence-out <file>	writes move sequence to a file. (default: if not specified, move sequence will not be saved to a file)
#
# --render <true>				determines whether to render frames during game play or to play game without visuals.
#								Can be useful when running on a server where no graphical interface is supported.
#								Can be useful for testing multiple game plays efficiently without the delay of rendering frames.
#
# --delay <time>				Delay between frames in seconds. Raise this to slow down game play. Subtracted from feed forward time. (default 0.005 seconds)
# 
# --save-playback				File to save playback video to. Independent of render. (default: none)
# --device <cpu|gpu>			Determines whether to use CPU or GPU for feed forwards. (default: GPU if available, CPU otherwise)
#
# --duration <time>				Sets how long to play for (default: "game over", plays until game over)

parser = argparse.ArgumentParser(description='Training Argument')

parser.add_argument('--agent_type', default='dqn', choices=['ac', 'neat', 'dqn', 'random', 'replay'], help='select agent to train (default: %(default)s)')
parser.add_argument('--level', type=int, choices=range(1,17), help='select agent to train (default: %(default)s)')
parser.add_argument('--render', action='store_true', help='renders env during play')
parser.add_argument('--record', action='store_true', help='records env during play')
parser.add_argument('--duration', default=9999, type=int, choices=range(0,9999), help='select agent to train (default: %(default)s)')
parser.add_argument('--ckpt', default=None, type=str, help='select ckpt to play, MUST USE RELATIVE PATH (default: None)') 
parser.add_argument('--seg', default=None, type=str, help='select segmentation model, MUST USE RELATIVE PATH (default: None)') 

args = parser.parse_args()

def main():
	
	print("=== Play Game ===")
 
	# ---Initialize objects ---
	random.seed(time.time())	# Use a time based seed

	# --- Level ---
	level = None
	if args.level:
		level = Levels.select(args.level-1)
	else:
		level = Levels.random()

	# --- Agent and Model ---
	if args.agent_type == 'ac':
		agent = ActorCritic()
	elif args.agent_type == 'random':
		agent = RandomAgent()
	elif args.agent_type == 'neat':
		agent = NeatAgent()
	elif args.agent_type == 'replay':
		agent = ReplayAgent()
	else:
		agent = DeepQ(args.ckpt, args.seg)
	print("Agent:", agent.name())

	print("Level:", level.game_name(), level.to_state())
	if args.record:
		ckpt = Checkpoint()
		ckpt.agent_name = agent.name()
		ckpt.make_dir(mode=Checkpoint.PLAYBACK_MODE)
		record_dir = ckpt.generate_dir(mode=Checkpoint.PLAYBACK_MODE)
	else:
		record_dir = False

	# --- 4.) Load its trained data (or initialize untrained model) ---
	if args.ckpt is not None:
		agent.load(args.ckpt)

	env = retro.make(game=level.game_name(), state=level.to_state(), record=record_dir)
	ob = env.reset()

	# --- 5.) Play ---
	ob, rew, done, info = env.step(ActionSpace.stand_still())	# make one passive move to initialize ob and info
	
	# Repeat until game over
	done = False
	startTime = time.time()
	counter = 0
	
	while not done and (time.time() - startTime < args.duration):
		# --- 1.) Show Frame ---
		if args.render:
			#env.render()
			original = cv.cvtColor(ob, cv.COLOR_RGB2BGR)
			segmented = None

			if args.seg is not None:
				segmented = agent.seg_model.segment(ob)
				segmented = segmented.astype(np.uint8)	# convert from float64 pixels to uint8

			imageTuple = ImageTuple(original, segmented)
			cv.imshow("supersonicai", imageTuple.combine())	# show original and segmented in one window.

		# --- 2.) Make Decision ---
		buttons = agent.decide(ob, info)
		
		ob, rew, done, info = env.step(buttons)
		
		# Pause between frames
		#time.sleep(0.005)
		#time.sleep(0.01)
		cv.waitKey(1)
		
		counter += 1

# Call main
	
if __name__ == '__main__':	

	main()

