##---------------Source-------------------------##
# Montalvo, J., García-Martín, Á. & Bescós, J. Exploiting semantic segmentation to boost reinforcement learning in video game environments. 
# Multimed Tools Appl (2022). https://doi-org.ezproxy.lib.vt.edu/10.1007/s11042-022-13695-1import 
##---------------Source-------------------------##
import os
import sys

os.environ['CUDA_LAUNCH_BLOCKING'] = '1'
script_dir = os.path.dirname(os.path.abspath(__file__))
project_dir = os.path.abspath(script_dir + "/../..")


sys.path.append(os.path.abspath(project_dir + '/source/datasets'))
sys.path.append(os.path.abspath(project_dir + '/source/vision'))

from deeplab import *
from deeplab_dataset import *

from PIL import Image
import numpy as np
import random
import argparse
import time
from os.path import join
from tqdm import tqdm

import torch


parser = argparse.ArgumentParser()
parser.add_argument("-m","--model",default=None,type=str, help="Name of a partially trained model. Training will continue to optimize these set of weights.")
parser.add_argument("-o","--output_file",default="SegmentationModel",type=str, help="Name of the model. Will be saved on results/deeplab_ckpts")
parser.add_argument("-bs","--batch_size",default=4, choices=range(2,32),type=int, help="Keep it always 2 or more, otherwise it will crash.") ## 
parser.add_argument("-d","--dataset", default='data/segmentation_dataset', help="Path to dataset",type=str)
parser.add_argument("-e","--epochs",default=45,type=int,help="Epochs")
args = parser.parse_args()

def main():

    deep_lab = DeepLab(args.model)
    trainset = SonicDataset(args, 'train')
    train_loader = torch.utils.data.DataLoader(trainset, batch_size=args.batch_size, shuffle=True, num_workers=deep_lab.workers, pin_memory=True)

    testset = SonicDataset(args, 'val')
    test_loader = torch.utils.data.DataLoader(testset, batch_size=args.batch_size, shuffle=False, num_workers=deep_lab.workers, pin_memory=True)

    loss_train_epoch = []
    loss_test_epoch = []
    acc_train_per_epoch = []
    acc_test_per_epoch = []
    new_labels = []
    path = os.path.join(project_dir, "results", "deeplab_ckpts")
    if not os.path.isdir(path):
        os.makedirs(path)

    for epoch in tqdm(range(1, args.epochs + 1), desc = f"DeepLabV3_Resnet50 training"):
        st = time.time()
        loss_per_epoch = deep_lab.train_epoch(args, train_loader)

        loss_train_epoch += [loss_per_epoch]

        deep_lab.scheduler.step()

        loss_per_epoch_test, acc_val_per_epoch_i = deep_lab.testing(test_loader)

        loss_test_epoch += loss_per_epoch_test
        acc_test_per_epoch += [acc_val_per_epoch_i]

        if epoch == 1:
            best_acc_val = acc_val_per_epoch_i
            
        else:
            if acc_val_per_epoch_i > best_acc_val:
                best_acc_val = acc_val_per_epoch_i

        
        torch.save(deep_lab.model.state_dict(), os.path.join(path, f'{args.output_file}_{epoch}.pt'))


# Call main
main()