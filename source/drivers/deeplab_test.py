##---------------Source-------------------------##
# Montalvo, J., García-Martín, Á. & Bescós, J. Exploiting semantic segmentation to boost reinforcement learning in video game environments. 
# Multimed Tools Appl (2022). https://doi-org.ezproxy.lib.vt.edu/10.1007/s11042-022-13695-1import 
##---------------Source-------------------------##
import os
import sys

script_dir = os.path.dirname(os.path.abspath(__file__))
project_dir = os.path.abspath(script_dir + "/../..")


sys.path.append(os.path.abspath(project_dir + '/source/datasets'))
sys.path.append(os.path.abspath(project_dir + '/source/vision'))

from deeplab import *
from deeplab_dataset import *
import argparse




parser = argparse.ArgumentParser()
parser.add_argument("-m","--model",default='results/deeplab_ckpts/SegmentationModel.pt',type=str, help="Name of the model. Will be saved on results/deeplab_ckpts")
parser.add_argument("-i","--image",required=True, help="Path to image",type=str)
args = parser.parse_args()

def main():
    seg = DeepLab(args.model)
    seg.seg_test(os.path.join(project_dir, args.image))
# Call main
main()

