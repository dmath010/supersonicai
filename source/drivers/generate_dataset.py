# This script generates a dataset of images used for image segmentation.
# The script takes a list of .png files
# The script outputs .png images in a specified directory
# Works differently for sprites vs maps
# 
# For sprites:
#	- crops each sprite into a cv.Mat using coorinates specified in the coordinates files
#	- resizes cv.Mat to fit the cropped sprite
#	- outlines the foregrown as 1 and background as 0
#	- writes to a .png file
#	- creates a .cvs document outlining the directories of each image file and their description
#
# For maps:
#	- ???
#

import sys
import os
import cv2 as cv
import numpy as np

script_dir = os.path.dirname(os.path.abspath(__file__))
project_dir = os.path.abspath(script_dir + "/../..")

sys.path.append(os.path.abspath(project_dir + '/source'))
sys.path.append(os.path.abspath(project_dir + '/source/datasets'))	# add datasets directory

from globals import * 
from image_file_lookup import *
from image_assembler import *
from segmentation_labels import *
from background import * 
from tilesets import * 
from sprites import *
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("-n","--num_images", default=100,type=int,choices=range(10,40001), help="Number of Images to Generate")

args = parser.parse_args()

def main():
	counter = 0			# How many samples have we generated already?
	image_limit = args.num_images	# How many samples should we generate?
	path_to_dir = os.path.join(project_dir, "results", "datasets", "segmentation_dataset")

	if not os.path.isdir(path_to_dir):
		os.makedirs(path_to_dir)
		os.makedirs(os.path.join(path_to_dir, 'originals'))
		os.makedirs(os.path.join(path_to_dir, 'segmentations'))
		os.makedirs(os.path.join(path_to_dir, 'colorized'))
	
	print("--- Generating Image Segmentation Dataset ---")
	print("Generating", image_limit, "image samples to", path_to_dir)

	sonics = Sprites.Sonic()
	sonics.load()
	badniks = Sprites.Badniks()
	badniks.load()

	while True:
		
		# --- Load required images from files ---
		background = Background.select_random()
		background.virtual_load()
		
		#tileset = Tilesets.select(3)
		tileset = Tilesets.select_random()
		tileset.load()
	
		for i in range(5):
	
			# --- 0.) Create blank image ---
			final = ImageTuple(
				original = np.zeros(globals.IMAGE_SIZE_3D, dtype=np.uint8),
				segmented = np.zeros(globals.IMAGE_SIZE_2D, dtype=np.uint8),
				background_mask = np.zeros(globals.IMAGE_SIZE_2D, dtype=np.uint8),
				foreground_mask = np.zeros(globals.IMAGE_SIZE_2D, dtype=np.uint8),
			)

			# --- 1.) Generate a Background ---
			bg = background.virtual_stitch()
			#cv.imshow("background original", bg.original)
			#cv.imshow("background segmented", bg.segmented)
			#cv.imshow("background fg mask", bg.foreground_mask)
			#cv.imshow("background bg mask", bg.background_mask)
			#cv.waitKey(0)

			final.original = bg.original
			final.segmented = bg.segmented
			#cv.imshow("original", final.original)
			#cv.imshow("segmented", final.segmented * SegmentationLabels.N_LABELS)
			#cv.waitKey(0)

			# --- 2-1.) Generate a Tileset Sprite ---
			ts = tileset.stitch()
			#cv.imshow("tileset original", ts.original)
			#cv.imshow("tileset segmented", ts.segmented * SegmentationLabels.N_LABELS)
			#cv.imshow("tileset fg_mask", ts.foreground_mask)
			#cv.imshow("tileset bg_mask", ts.background_mask)
			#cv.waitKey(0)

			# --- 2-2.) Overlay Tileset onto Final Image ---
			final.original = cv.bitwise_and(final.original, final.original, mask=ts.foreground_mask)
			ts.original = cv.bitwise_and(ts.original, ts.original, mask = ts.background_mask)
			
			final.segmented = cv.bitwise_and(final.segmented, final.segmented, mask=ts.foreground_mask)
			ts.segmented = cv.bitwise_and(ts.segmented, ts.segmented, mask = ts.background_mask)
			#cv.imshow("original cutout", final.original)
			#cv.imshow("tileset cutout", ts.original)
			
			final.original = cv.add(final.original, ts.original)
			final.segmented = cv.add(final.segmented, ts.segmented)
			#cv.imshow("original", final.original)
			#cv.imshow("segmented", final.segmented * SegmentationLabels.N_LABELS)

			# --- 3.) Generate a Sonic Sprite ---
			s = sonics.stitch()
			final.original = overlay_images(final.original, s.original, s.background_mask)
			final.segmented = overlay_images(final.segmented, s.segmented, s.background_mask)
			#cv.imshow("sonics", s.combine())

			# --- 4.) Generate Item Sprites ---
	
			# --- 5.) Generate Robot Sprites ---
			b = badniks.stitch()
			final.original = overlay_images(final.original, b.original, b.background_mask)
			final.segmented = overlay_images(final.segmented, b.segmented, b.background_mask)
			#cv.imshow("badniks", b.combine())

			# --- 6.) Generate Hazard Sprites ---
			
			# --- 7.) Combine into a final sample ---
			
			#cv.imshow("final sample", final.combine())
			#cv.waitKey(0)

			# --- 8.) Save to a file ---
			final.save(path_to_dir, str(counter))

			counter += 1

			# Keep both counter checks
			if counter > image_limit:
				return

			#cv.waitKey(0)
	
		cv.destroyAllWindows()

		# --- Free memory ---
		background.unload()
		tileset.unload()

		# Keep both counter checks
		if counter > image_limit:
			return
	
main()
