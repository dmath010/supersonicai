import os
import sys
import torch

script_dir = os.path.dirname(os.path.abspath(__file__))
project_dir = os.path.abspath(script_dir + "/../..")

sys.path.append(os.path.abspath(project_dir + '/source/agents'))
sys.path.append(os.path.abspath(project_dir + '/source/interface'))
sys.path.append(os.path.abspath(project_dir + '/source/learning'))

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

print(f"Is CUDA supported by this system? {torch.cuda.is_available()}")
print(f"CUDA version: {torch.version.cuda}")

# Storing ID of current CUDA device
cuda_id = torch.cuda.current_device()
print(f"ID of current CUDA device: {torch.cuda.current_device()}")

print(f"Name of current CUDA device: {torch.cuda.get_device_name(cuda_id)}")

print("\n--- Call nvidia-smi in another terminal to test GPU usage ---\n")

tensor1 = torch.rand(10000)
tensor2 = torch.rand(10000)

tensor1 = tensor1.to(device)
tensor2 = tensor2.to(device)

print("Calling tensor add operations on device:", device)

for i in range(0, 300):
	tensor1 = tensor1 + tensor2

