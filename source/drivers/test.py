import sys
import os
import cv2 as cv
import numpy as np

script_dir = os.path.dirname(os.path.abspath(__file__))
project_dir = os.path.abspath(script_dir + "/../..")

#sys.path.append(os.path.abspath(project_dir + '/source/agents'))
sys.path.append(os.path.abspath(project_dir + '/source/interface'))
#sys.path.append(os.path.abspath(project_dir + '/source/learning'))
sys.path.append(os.path.abspath(project_dir + '/source/datasets'))

#from levels import *
#from all_agents import *
from background import *
from sprites import *

bg = GreenHillBackground()
bg.virtual_load()
pair = bg.virtual_stitch()
cv.imshow("bkg", pair.original)
cv.waitKey(0)
cv.destroyAllWindows()

sp = Badniks()
sp.load()
pair = sp.get_all_mask()

#t = torch.rand([2000000000], dtype=torch.float32)
#print(t)
# 8GB for 2'000'000'000 floats		
