import sys
import os
import cv2 as cv
import numpy as np

script_dir = os.path.dirname(os.path.abspath(__file__))
project_dir = os.path.abspath(script_dir + "/../..")

sys.path.append(os.path.abspath(project_dir + '/source/datasets'))	# add datasets directory

from image_file_lookup import *
# Thresholding: https://www.geeksforgeeks.org/getting-started-with-python-opencv/

class opencv_functions:
	def functions():
		# --- Create Blank Image ---
		# https://pupli.net/2019/03/create-blank-image-using-opencv-python/#:~:text=Create%20blank%20image%20using%20OpenCV%20Python%20March%204%2C,%3D%20np.zeros%28shape%3D%5B512%2C%20512%2C%203%5D%2C%20dtype%3Dnp.uint8%29%20%23%20print%20%28blank_image.shape%29
		##image = np.zeros(shape=[224, 340, 3], dtype=np.uint8)	# creates a black RGB image of size 224 rows and 340 columns
		
		# --- Show on screen ---
		##cv.imshow("window name", image)
		##cv.waitKey(0)		
		##cv.destroyWindow("window name")
		##cv.destroyAllWindows()

		# --- Save to file ---
		#cv.imwrite("filename", image)

		# --- Read from file ---
		#img = cv.imwrite("filename")

		# --- Resize Image ---
		# https://www.geeksforgeeks.org/getting-started-with-python-opencv/
		##half = cv.resize(image, (0, 0), fx = 0.1, fy = 0.1)
		##bigger = cv.resize(image, (1050, 1610))

		# --- Color Space ---
		# RBG to HSV
		##hsv = cv.cvtColor(image, cv.COLOR_BGR2HSV)

		# --- Drawing Operations using OpenCV ---
		# https://aihints.com/how-to-draw-points-in-opencv-python/
		img = np.zeros(shape=[224, 340, 3], dtype=np.uint8)
		mask = np.zeros(shape=img.shape, dtype=img.dtype)
		
		# Draw a red circle:
		#	- centered on col 50 row 20
		#	- with radius 10 pixels
		#	- -1 for filled circle
		img = cv.circle(img, (50, 20), radius=10, color=(0, 0, 255), thickness=-1)
		cv.imshow("circle", img)
		cv.waitKey(0)
		cv.destroyAllWindows()

		# --- Element-wise Operations using OpenCV (as opposed to numpy) ---
		
		cv.add(img, mask)

	def copy_to():
		# 0.) Create a blank image
		image = np.zeros(shape=[512, 512, 3], dtype=np.uint8)
		image2 = np.zeros(shape=[200, 200, 3], dtype=np.uint8)

		# --- Slow way ---
		for r in range(0, image.shape[0]):
			for c in range(0, image.shape[1]):
				image[r][c][0] = r + c
				image[r][c][1] = r + c
				image[r][c][2] = r + c

		for r in range(0, image2.shape[0]):
			for c in range(0, image2.shape[1]):
				image2[r][c][0] = r & c
				image2[r][c][1] = r & c
				image2[r][c][2] = r & c

		# --- Fast way ---
		
		# --- Copy To ---
		# Copies matricies of different sizes.
		# takes pixels from rows 20:40 and cols 20:40 from image2
		# and copies them into rows 20:40 and cols 20:40 from image
		image[20:40, 20:40] = image2[20:40, 20:40]

		cv.imshow("image", image)
		cv.imshow("image2", image2)
		cv.waitKey(0)
		cv.destroyAllWindows()

	def image_reconstruction():
		# 0.) Create a blank image
		image = np.zeros(shape=[224, 340, 3], dtype=np.uint8)
		
		for row in range(0, image.shape[0]):
			for col in range(0, image.shape[1]):
				image[row, col] = row & col

		# 1.) Load a background image
		img_dir = os.path.join(project_dir, "data/SpritersResource")
		lookup = ImageFileLookup(img_dir)
		spring_yard_zone = cv.imread(lookup.backgrounds.path_to_spring_yard_zone_bg)

		# 2.) Crop parts of spring yard zone
		# upper left corner (row, col)
		origin1 = (448, 24)
		origin2 = (536, 24)
		origin3 = (640, 24)

		# lower right corner (row, col)
		point1 = (527, 340)
		point2 = (631, 340)
		point3 = (655, 340)

		# (n rows, n cols)	or	(height, width)
		size1 = (point1[0] - origin1[0], image.shape[1])
		size2 = (point2[0] - origin2[0], image.shape[1])
		size3 = (point3[0] - origin3[0], image.shape[1])

		background1 = spring_yard_zone[origin1[0]:origin1[0] + size1[0], origin1[1]:origin1[1] + size1[1]]
		background2 = spring_yard_zone[origin2[0]:origin2[0] + size2[0], origin2[1]:origin2[1] + size2[1]]
		background3 = spring_yard_zone[origin3[0]:origin3[0] + size3[0], origin3[1]:origin3[1] + size3[1]]
		
		#shape1 = background1.shape
		#shape2 = background2.shape
		#shape3 = background3.shape

		#cv.imshow("bg1", background1)
		#cv.imshow("bg2", background2)
		#cv.imshow("bg3", background3)
		
		# 3.) Copy Background onto the blank image

		row = 0
		image[row:row + size1[0],0:size1[1]] = background1
		row += size1[0]
		image[row:row + size2[0],0:size2[1]] = background2
		row += size2[0]
		image[row:row + size3[0],0:size3[1]] = background3
		row += size3[0]

		cv.imshow("spring yard zone", image)
		#cv.waitKey(0)
		#cv.destroyAllWindows()

		# 4.) Copy Stage onto image
		full_stage = cv.imread(lookup.stages.path_to_spring_yard_zone_act1_stage, cv.IMREAD_COLOR)
		stage = full_stage[1050:1050+image.shape[0], 2860:2860+image.shape[1]]
		
		# stage contains pixels which need to be transparent
		#stage_hsv = cv.cvtColor(stage, cv.COLOR_BGR2HSV)
		#stage_hue = stage_hsv[:, :, 0]	# hue channel only 
		stage_red = stage[:, :, 2]		# red channel only
		stage_green = stage[:, :, 1]	# green channel only
		stage_blue = stage[:, :, 0]		# blue channel only

		_, mask_red_lower = cv.threshold(stage_red, 130, 255, cv.THRESH_BINARY)		# Set 1's for all pixels which are less than purple
		_, mask_red_upper = cv.threshold(stage_red, 135, 255, cv.THRESH_BINARY_INV)		
		mask_bot = cv.bitwise_and(mask_red_lower, mask_red_upper)									# Set 1's for all pixels which are purple
		mask_top = cv.bitwise_not(mask_bot)

		stage_pixels = cv.bitwise_and(stage, s4tage, mask=mask_top)			# cut silouette of stage
		image = cv.bitwise_and(image, image, mask=mask_bot)					# cut silouette of stage

		image = cv.add(image, stage_pixels)

		cv.imshow("mask_top", mask_top)
		cv.imshow("mask_bot", mask_bot)
		cv.imshow("stage_pixels", stage_pixels)
		#cv.imshow("stage_red", stage_red)
		#cv.imshow("mask_red_lower", mask_red_lower * 255)
		#cv.imshow("mask_red_upper", mask_red_upper * 255)
		cv.imshow("stage", stage)
		cv.imshow("image", image)
		cv.waitKey(0)
		cv.destroyAllWindows()

#opencv_functions.functions()
opencv_functions.image_reconstruction()



