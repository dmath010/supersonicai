
class Color:
	def __init__(self, red, green, blue):
		self.red = red
		self.green = green
		self.blue = blue
	
	def __str__(self):
		return "".join(["(", str(self.red), ", ", str(self.green), ", ", str(self.blue), ")"])

	def toTuple(self):
		return (self.blue, self.green, self.red)

	def make_red():
		return Color(255, 0, 0)	
	
	def make_green():
		return Color(0, 255, 0)
	
	def make_blue():
		return Color(0, 0, 255)

	def make_light_gray():
		return Color(200, 200, 200)

	def make_dark_gray():
		return Color(100, 100, 100)

	def make_brown():
		return Color(120, 67, 21)
	
	def make_purple():
		return Color(191, 41, 204)

	def make_yellow():
		return Color(255, 253, 85)

	def make_orange():
		return Color(255, 156, 0)