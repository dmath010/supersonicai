
class AQueue:
	def __init__(self, capacity = 10):
		self.__data = [ None ] * capacity
		self.__begin = 0
		self.__end = 0
	
	# Returns index of 1 after 'index'.
	# usually returns 'index' + 1
	# Wraps around to 0 when end of array is reached
	def __increment(self, index) -> int:
		return (index + 1) % self.capacity()

	def __decrement(self, index) -> int:
		i = (index - 1) 
		if i < 0:
			i = self.size() - 1
		return i

	# re allocates a bigger array for queue
	# and copies original element over to it. Does not 
	def reserve(self, capacity) -> int:
		if capacity > self.capacity():
			size = self.size()
			data = [ None ] * capacity

			i = self.__begin
			j = 0
			while i != self.__end and j < len(data):
				data[j] = self.__data[i]
				i = self.__increment(i)
				j += 1

			self.__data = data
			self.__begin = 0
			self.__end = size

	# Number of elements which can fit in queue before its internal array needs to be reallocated to a bigger one
	def capacity(self) -> int:
		return len(self.__data)

	# Number of elements present in queue
	def size(self) -> int:
		size = self.__end - self.__begin

		if size < 0:
			size = self.capacity() - size
		
		return size

	def empty(self) -> bool:
		return self.size() == 0
	
	def push(self, value) -> None:
		if self.size() + 1 < self.capacity():
			self.reserve(self.size() * 2)
		
		self.__data[self.__end] = value
		self.__end = self.__increment(self.__end)
		
	def pop(self):
		value = self.__data[self.__begin]
		self.__data[self.__begin] = None
		self.__begin = self.__increment(self.__begin)
		return value
	
	# Returns the "oldest" element in the queue
	def front(self):
		return self.__data[self.__begin]

	# Returns the "newest" element in the queue
	def back(self):
		i = self.__decrement(self.__end)

		return self.__data[i]

	def clear(self) -> None:
		i = self.__begin

		while i != self.__end:
			self.__data[i] = None
			i = self.__increment(i)
		
		self.__begin = 0
		self.__end = 0
	
	def __str__(self) -> str:
		s = "["

		i = self.__begin
		while i != self.__end:
			if i != self.__begin:
				s += ", "
			
			s += str(self.__data[i])
			i = self.__increment(i)

		s += "]"

		return s