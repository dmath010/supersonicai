# Actor Critic using Cart Pole: https://github.com/yc930401/Actor-Critic-pytorch/blob/master/Actor-Critic.py
# Policy Gradient RL: https://medium.com/@thechrisyoon/deriving-policy-gradients-and-implementing-reinforce-f887949bd63
# 

import sys 
import os
import retro
import time
import torch 
import torch.optim as optim
import cv2 as cv

script_dir = os.path.dirname(os.path.abspath(__file__))
project_dir = os.path.abspath(script_dir + "/../..")
data_dir = os.path.abspath(script_dir + '/../../data')	# TODO: 

sys.path.append(os.path.abspath(project_dir + '/source/agents'))
sys.path.append(os.path.abspath(project_dir + '/source/interface'))
sys.path.append(os.path.abspath(project_dir + '/source/learning'))

from action_space import *
from reward_system import * 

# Traines/Optimizes and Actor Critic Agent using RL.
class ActorCriticTrainer:
	def compute_returns(next_value, rewards, masks, gamma=0.99):
		R = next_value
		size = rewards.size()[0]
		returns = torch.zeros(size, device=rewards.device)
		for step in reversed(range(len(rewards))):
			R = rewards[step] + gamma * R * masks[step]
			returns[step]

		return returns
	
	def train_actor_critic(agent, env, n_epochs, reward, render, checkpoint, save_rate):
		print("=== Training Actor Critic ===")
		
		optimizerA = optim.Adam(agent.actor.parameters(), lr = 0.02)
		optimizerC = optim.Adam(agent.critic.parameters(), lr = 0.02)

		device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

		agent.to(device)

		state = env.reset()

		reward_system = RewardSystem()
		
		# In each iteration, the nn is optimized once.
		for epoch in range(n_epochs):
			#print("--- epoch =", epoch, "---")

			if epoch % 50 == 0:
				state = env.reset()
				
			# If n_steps is too large, it can cause memory errors
			n_steps = 200
			log_probs = torch.zeros(n_steps, device=device)
			values = torch.zeros(n_steps, device=device)
			rewards = torch.zeros(n_steps, device=device)
			masks = torch.zeros(n_steps, device=device)
			entropy = 0

			running_reward = 0
			next_state, reward, done, info = env.step(ActionSpace.stand_still())	# make a passive move to initialize values

			# --- Initialize Reward System with current game ---
			reward_system.init(info)
			reward = reward_system.calc_reward(info, ActionSpace.stand_still())

			jump_counter = 0

			for i in range(n_steps):
				if render:
					env.render()

				# --- Forward Pass ---
				#state = cv.resize(state, [int(244/4), int(320/4)])
				#cv.imshow("img", state)
				#cv.waitKey(1)
				state = torch.FloatTensor(state).to(device)
				#print(state.size())
				dist = agent.actor(state)
				value = agent.critic(state)

				action = dist.sample()
				buttons = ActionSpace.move(action)
				next_state, reward, done, info = env.step(buttons)
				reward = reward_system.calc_reward(info, buttons)
				
				if ActionSpace.is_jump(buttons):
					jump_counter += 1

				if done:
					state = env.reset()
					next_state, reward, done, info = env.step(ActionSpace.stand_still())	# make a passive move to initialize values
					reward = reward_system.calc_reward(info, ActionSpace.stand_still())

				# --- Calc Reward ---
				running_reward += reward
				log_prob = dist.log_prob(action).unsqueeze(0)
				entropy += dist.entropy().mean()

				# --- Accumulate Data for Loss ---
				log_probs[i] = log_prob
				values[i] = value
				rewards[i] = reward
				masks[i] = 1-done

				state = next_state

				if done:
					print('Epoch: {}, Score: {}'.format(epoch, i))
					break

			print("Epoch:", epoch, "\trunning_reward:", running_reward, "\taverage reward per step:", running_reward / n_steps)

			#next_state = cv.resize(next_state, [int(244/4), int(320/4)])
			next_state = torch.FloatTensor(next_state).to(device)
			next_value = agent.critic(next_state)
			returns = ActorCriticTrainer.compute_returns(next_value, rewards, masks)

			advantage = returns - values

			actor_loss = -(log_probs * advantage.detach()).mean()
			critic_loss = advantage.pow(2).mean()
			
			optimizerA.zero_grad()
			optimizerC.zero_grad()
			actor_loss.backward()
			critic_loss.backward()
			optimizerA.step()
			optimizerC.step()

			if epoch % 1000 == 0:
				torch.save(agent.actor, 'actor_' + str(epoch) + '.pt')
				torch.save(agent.critic, 'critic_' + str(epoch) + '.pt')

		torch.save(agent.actor, 'actor.pt')
		torch.save(agent.critic, 'critic.pt')

		env.close()
