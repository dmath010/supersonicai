
##---------------Sources-------------------------##
# DeepQ Learning with PyTorch: https://pytorch.org/tutorials/intermediate/reinforcement_q_learning.html
# DeepQ Image Processing for GymRetro:  https://github.com/deepanshut041/Reinforcement-Learning 
# Helper Functions for Gym Retro: https://github.com/moversti/sonicNEAT 
##------------------------------------------------##

from fileinput import filename
import time
import retro
import random
import torch
import numpy as np
from collections import deque
import math
import os
import sys
from datetime import datetime, date

script_dir = os.path.dirname(os.path.abspath(__file__))
project_dir = os.path.abspath(script_dir + "/../..")

sys.path.append(os.path.abspath(project_dir + '/source/agents'))
sys.path.append(os.path.abspath(project_dir + '/source/interface'))
sys.path.append(os.path.abspath(project_dir + '/source/learning'))
sys.path.append(os.path.abspath(project_dir + '/source/models'))
sys.path.append(os.path.abspath(project_dir + '/source/vision'))

from all_agents import * 
from checkpoint import *
from deep_q_agent import *
from deep_q_model import DQN
from image_processing import preprocess_frame, stack_frame
from action_space import *
from reward_system import *

class DeepQTrainer:
	def stack_frames(frames, state, is_new=False, seg_model=None):
		"""Stacks frames for broader input of environment."""

		frame = preprocess_frame(state, seg_model)
		frames = stack_frame(frames, frame, is_new)
		return frames


	def train(agent, env, n_episodes=1000, reward_system=RewardSystem.Contest, render=False, ckpt=None, save_rate=10, seg_model=None):
		"""
		Params
		======
			n_episodes (int): maximum number of training episodes
		"""
		# if gpu is to be used
		device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
		print("Device: ", device)

		UPDATE_TARGET = 10000	# After which thershold replay to be started 
		EPS_START = 0.99		# starting value of epsilon
		EPS_END = 0.01			# Ending value of epsilon
		EPS_DECAY = 100			# Rate by which epsilon to be decayed

		# Initialize Agent
       
		start_epoch = 0
		scores = []
		best_ckpt_score = -9999999	# initialize to a very small number
		scores_window = deque(maxlen=20)

		# Initialize checkpoint
		ckpt = Checkpoint(agent)
		ckpt.make_dir() # Makes new directory if it does not exist

		epsilon_by_epsiode = lambda frame_idx: EPS_END + (EPS_START - EPS_END) * math.exp(-1. * frame_idx /EPS_DECAY)
		for i_episode in range(start_epoch + 1, n_episodes+1):
			state = DeepQTrainer.stack_frames(None, env.reset(), True, seg_model)
			next_state, reward, done, info = env.step(ActionSpace.stand_still())	# make a passive move to initialize data

			score = 0
			eps = epsilon_by_epsiode(i_episode)
			reward_system.init(info)

			# Punish the agent for not moving forward
			prev_state = {}
			steps_stuck = 0
			timestamp = 0

			while timestamp < 5000:
				action = agent.act(state, eps)
				next_state, reward, done, info = env.step(ActionSpace.move(action))
				reward = reward_system.calc_reward(info, ActionSpace.move(action))

				if render is True:
					env.render()

				score += reward

				timestamp += 1

				# Punish the agent for standing still for too long.
				if (prev_state == info):
					steps_stuck += 1
				else:
					steps_stuck = 0
				prev_state = info

				if (steps_stuck > 20):
					reward -= 1

				next_state = DeepQTrainer.stack_frames(state, next_state, False, seg_model)
				agent.step(state, action, reward, next_state, done)
				state = next_state
				if done:
					break
			
			scores_window.append(score)		# save most recent score
			scores.append(score)			# save most recent score
			print ("epoch:", i_episode, "score:", score)
		
			if (i_episode % save_rate == 0 and score > best_ckpt_score):
				ckpt.epoch = i_episode
				ckpt.score = score
				best_ckpt_score = score
				fn = ckpt.generate_path()
				agent.save(fn, i_episode)
				print(f"Saving checkpoint with new best score {best_ckpt_score}")
			
		return scores

