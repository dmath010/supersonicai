import sys
import os

script_dir = os.path.dirname(os.path.abspath(__file__))
project_dir = os.path.abspath(script_dir + "/../..")

sys.path.append(os.path.abspath(project_dir + '/source/learning'))

from train_actor_critic import *
from train_brute import *
from train_deep_q import * 
from train_neat import *

