
##---------------Sources-------------------------##
# Neat NN Implementation: https://gitlab.com/lucasrthompson/Sonic-Bot-In-OpenAI-and-NEAT
# DeepQ Image Processing for GymRetro:  https://github.com/deepanshut041/Reinforcement-Learning 
# Helper Functions for Gym Retro: https://github.com/moversti/sonicNEAT 
##-----------------------------------------------##

import retro
import numpy as np
import cv2
import neat
import pickle
import sys
import os

script_dir = os.path.dirname(os.path.abspath(__file__))
project_dir = os.path.abspath(script_dir + "/../..")

sys.path.append(os.path.abspath(project_dir + '/source/agents'))
sys.path.append(os.path.abspath(project_dir + '/source/interface'))
sys.path.append(os.path.abspath(project_dir + '/source/vision'))

from configparser import Interpolation
from inspect import getsourcefile

# Trains a NEAT NN.
def train_neat():
    env = retro.make(game="SonicTheHedgehog-Genesis", state="GreenHillZone.Act1", scenario="contest", record='.')
    imgarray = []
    xpos_end = 0

    SEE_NETWORK_INPUT=True

    resume = True
    restore_file = "neat-checkpoint-32"


    def eval_genomes(genomes, config):
        for genome_id, genome in genomes:
            ob = env.reset()
            ac = env.action_space.sample()

            inx, iny, inc = env.observation_space.shape

            inx = int(inx / 8)
            iny = int(iny / 8)

            net = neat.nn.recurrent.RecurrentNetwork.create(genome, config)

            current_max_fitness = 0
            fitness_current = 0
            frame = 0
            counter = 0
            xpos = 0

            done = False

            while not done:

                env.render()
                frame += 1
                ob = cv2.resize(ob, (inx, iny))
                #ob = cv2.blur(ob, (3,3))
                ob = cv2.cvtColor(ob, cv2.COLOR_BGR2GRAY)

                if SEE_NETWORK_INPUT:
                    img = ob.copy()
                    dst = (img.shape[0] * 8, img.shape[1] * 8)
                    img = cv2.resize(img, dst, interpolation=cv2.INTER_NEAREST)
                    img = np.flipud(img)

                ob = np.reshape(ob, (inx, iny))

                imgarray = np.ndarray.flatten(ob)

                nnOutput = net.activate(imgarray)
                ac = env.action_to_array(nnOutput)
                ob, rew, done, info = env.step(nnOutput)

                xpos = info['x']

                if xpos >= 60000:
                    fitness_current += 10000000
                    done = True

                fitness_current += rew

                if fitness_current > current_max_fitness:
                    current_max_fitness = fitness_current
                    counter = 0
                else:
                    counter += 1

                if done or counter == 250:
                    done = True
                    print(genome_id, fitness_current)

                genome.fitness = fitness_current

    # Get directory of current script. This directory is also the one contain 'config-feedforward'
    config_dir = os.path.dirname(getsourcefile(lambda:0))   

    config = neat.Config(neat.DefaultGenome, neat.DefaultReproduction,
                     neat.DefaultSpeciesSet, neat.DefaultStagnation,
                     'source/models/neat-feedforward')
    if resume == True:
        p = neat.Checkpointer.restore_checkpoint(restore_file)
    else:
        p = neat.Population(config)

    p.add_reporter(neat.StdOutReporter(True))
    stats = neat.StatisticsReporter()
    p.add_reporter(stats)
    p.add_reporter(neat.Checkpointer(1, filename_prefix='neat-checkpoint-'))

    winner = p.run(eval_genomes, 1)

    with open('winnerTEST.pkl', 'wb') as output:
        pickle.dump(winner, output, 1)

if __name__ == "__main__":
    train_neat()