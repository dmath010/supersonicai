
import numpy as np
import cv2 as cv
import os
import sys

script_dir = os.path.dirname(os.path.abspath(os.path.abspath(__file__)))	# absolute directory of current script's directory
project_dir = os.path.abspath(script_dir + "/../..")						# absolute directory of supersonicai 

sys.path.append(os.path.abspath(project_dir + '/source'))
sys.path.append(os.path.abspath(project_dir + '/source/datasets'))
sys.path.append(os.path.abspath(project_dir + '/source/vision'))	

from globals import *
from segmentation_labels import *
from image_processing import * 

class ImageTuple:
	def __init__(self, 
		original:np.ndarray=None,			# unsegmented image
		segmented:np.ndarray=None,			# segmented image
		background_mask:np.ndarray=None,		# pixels which are part of background
		foreground_mask:np.ndarray=None):	# pixels which are part of foreground (inverse of background)
		
		self.original = original
		self.segmented = segmented
		self.background_mask = background_mask
		self.foreground_mask = foreground_mask

		self.original = original if original is not None else np.zeros(globals.IMAGE_SIZE_3D, dtype=np.uint8)
		self.segmented = segmented if segmented is not None else np.zeros(globals.IMAGE_SIZE_2D, dtype=np.uint8)
		self.background_mask = background_mask if background_mask is not None else np.zeros((1, 1), dtype=np.uint8)
		self.foreground_mask = foreground_mask if foreground_mask is not None else np.zeros((1, 1), dtype=np.uint8)

	def colorize_segmentation(machine_segmented) -> np.ndarray:
		colorized_shape = (machine_segmented.shape[0], machine_segmented.shape[1], 3)
		colorized = np.zeros(colorized_shape, dtype=np.uint8)

		for label_index in range(0, SegmentationLabels.N_LABELS):
			c = SegmentationLabels.COLORS[label_index]

			mask = mask_by_intensity(machine_segmented, SegmentationLabels.LABELS[label_index])

			color = np.zeros(colorized.shape, dtype=colorized.dtype)
			color[:, :, 0] = c.blue
			color[:, :, 1] = c.green
			color[:, :, 2] = c.red

			colorized = overlay_images(colorized, color, mask)
			
		colorized = draw_legend(colorized)

		return colorized
	
	def save(self, path_to_directory, filename):

		filename_org = os.path.join(path_to_directory, "originals", filename + ".png")
		filename_seg = os.path.join(path_to_directory, "segmentations", filename + ".png")
		filename_col = os.path.join(path_to_directory, "colorized", filename + ".png")

		cv.imwrite(filename_org, self.original)
		cv.imwrite(filename_seg, self.segmented)
		cv.imwrite(filename_col, ImageTuple.colorize_segmentation(self.segmented))

	def combine(self) -> np.ndarray:
		# +--------------+--------------+
		# |   segmented  |   original   |
		# +--------------+--------------+
		# |   fg_mask    |   bg_mask    |
		# +--------------+--------------+
		
		# +--------------+--------------+
		# |   img1       |   img2       |
		# +--------------+--------------+
		# |   img3       |   img4       |
		# +--------------+--------------+
		
		colorized = ImageTuple.colorize_segmentation(self.segmented)
		fg_3channel = cv.cvtColor(self.foreground_mask, cv.COLOR_GRAY2BGR)
		bg_3channel = cv.cvtColor(self.background_mask, cv.COLOR_GRAY2BGR)

		# Shorted names by aliasing
		org = self.original
		clr = colorized
		fgm = fg_3channel
		bgm = bg_3channel

		img1 = clr
		img2 = org
		img3 = fgm
		img4 = bgm

		top_row = 0
		bot_row = max(img1.shape[0], img2.shape[0])
		lef_col = 0
		rig_col = max(img1.shape[1], img3.shape[1])

		total_rows = bot_row + max(img3.shape[0], img4.shape[0])
		total_cols = rig_col + max(img2.shape[1], img4.shape[1])
		
		big_img = np.zeros((total_rows, total_cols, 3), dtype=np.uint8)

		big_img[top_row:top_row + img1.shape[0], lef_col:lef_col + img1.shape[1], :] = img1
		big_img[top_row:top_row + img2.shape[0], rig_col:rig_col + img2.shape[1], :] = img2
		big_img[bot_row:bot_row + img3.shape[0], lef_col:lef_col + img3.shape[1], :] = img3
		big_img[bot_row:bot_row + img4.shape[0], rig_col:rig_col + img4.shape[1], :] = img4

		return big_img