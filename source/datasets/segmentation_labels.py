import os
import sys

script_dir = os.path.dirname(os.path.abspath(__file__))
project_dir = os.path.abspath(script_dir + "/../..")

sys.path.append(os.path.abspath(project_dir + '/source/vision'))

from color import * 

# Namespace/Lookup class which stores the values of each label used in image segmentation.
# Class is not ment to be instantiated into an object.
class SegmentationLabels:
	BACKGROUND1 = 0	# Sky, Mountains, Flowers, Waterfalls
	BACKGROUND2 = 1	# 
	STAGE = 2		# Objects in the level which we can walk on. Excludes pixels of pillars, trees, and underground. These count as background.
	SONIC = 3		# Sonic Himself
	ROBOTS = 4		# All Robots
	ITEMS = 5		# Good things:	Rings, Monitors (item boxes)
	HAZARDS = 6		# Bad things:	Spikes, fire, bullets, ...
	MECHANICAL = 7	# Interactible:	Springs, Rebounding Orbs, Fan, Rocks

	LABELS = [ 
		BACKGROUND1,
		BACKGROUND2,
		STAGE,
		SONIC,
		ROBOTS,
		ITEMS,
		HAZARDS,
		MECHANICAL,
	]
	N_LABELS = len(LABELS)

	BACKGROUND1_COLOR = Color.make_dark_gray()
	BACKGROUND2_COLOR = Color.make_light_gray()
	STAGE_COLOR = Color.make_brown()
	SONIC_COLOR = Color.make_blue()
	ROBOTS_COLOR = Color.make_purple()
	ITEMS_COLOR = Color.make_yellow()
	HAZARDS_COLOR = Color.make_red()
	MECHANICAL_COLOR = Color.make_orange()

	COLORS = [
		BACKGROUND1_COLOR,
		BACKGROUND2_COLOR,
		STAGE_COLOR,
		SONIC_COLOR,
		ROBOTS_COLOR,
		ITEMS_COLOR,
		HAZARDS_COLOR,
		MECHANICAL_COLOR,
	]

	# ----------------------- Sub Labels --------------------------------------
	# We might use sublabels in combination with one-hot encoding if it works.

	class RobotLabels:
		CRABMEAT = 1
		BUZZBOMBER = 2
		MOTOBUG = 3
		BASHER = 4
		NEWTRON = 5
		BOMB = 6
		CATERKILLER = 7
		SPIKES = 8
		BATBRAIN = 9
		ROLLER = 10
		BURROBOT = 11
		JAWS = 12
		ORBINAUT = 13
		BALLHOG = 14
		
	class ItemLabels:
		RING = 1
		YELLOW_SPRING = 2			# Weak spring
		RED_SPRING = 3				# Strong spring
		BUBBLE = 4					# Air bubble for underwater breathing
		RING_MONITOR = 5			# Item box for rings
		SPEED_MONITOR = 6 			# Item box for shoes
		FORCE_FIELD_MONITOR = 7		# Item box for force field
		INVINCIBILITY_MONITOR = 8	# Item box for invincibility
		ONE_UP_MONITOR = 9			# Item box for new life

	class HazardLabels:
		SPIKES = 1
		FLAMES = 2					# Fire, lava, exploxions, 
		RETRACTING_SPIKES = 3
		CRUSHER = 4
		SPIKE_FLAIL = 5				
	
	class MechanicalLabels:
		SWINGING = 1				# Includes chain and platform
		SPONGE = 2					#
		BUTTON = 3					#
		SWITCH = 4					#
		CHECKPOINT = 5				#
		GOALPOST = 6				#
		ANIMAL_CAPSULE = 7			# 
		TRAP_DOOR = 8				#
		FLAMETHROWER = 9			#
		SPARKING_CONDUIT = 10		#
		BROKEN_PIPE = 11			# 
		SPINING_FOOTHOLDS = 12		# 
		DOOR = 13					#
		RETRACTING_PLATFORM = 14	# 
		SEESAW = 15					#
		FAN = 16					#
		BOLDER = 17					# Doesn't move, but still fits.
		


