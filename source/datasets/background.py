from webbrowser import BackgroundBrowser
import numpy as np
import random
import cv2 as cv
import sys
import os

script_dir = os.path.dirname(os.path.abspath(os.path.abspath(__file__)))	# absolute directory of current script's directory
project_dir = os.path.abspath(script_dir + "/../..")						# absolute directory of supersonicai 

sys.path.append(os.path.abspath(project_dir + '/source'))	# add agents directory

from globals import * 
from image_file_lookup import ImageFileLookup
from image_crop_lookup import ImageCropLookup
from image_tuple import ImageTuple
from segmentation_labels import SegmentationLabels 

# ------------------------------------- ABSTRACT BASE CLASS -------------------

# Creates a full backrounds image cropped to (224 x 340 x 3) 
# *** Background images don't have explicit segmentations because all the pixels ***
# *** in the backgrounds are part of the same segment. ***
class BackgroundBase:
	# --- Methods ---

	def __init__(self):
		# Contains the components that make the background image.
		# Each strip can be stitched together from top to bottom for form the full background image.
		# There are 1 or more strips for every level.
		# The strips are parallax strips.
		# By offsetting the column cordinates, the background will appear to have depth.
		# Stips can have different sizes.
		# See greenhill zone for a good example of backgrounds constructed from multiple strips.
		# See marble zone for an example of a 1 strip background.
		# strips - list of ndarray	
		self.strips = []

	def virtual_load(self) -> None: # Empty for base class implementation
		return None
	
	# Deletes loaded data to save runtime memory. 
	def unload(self) -> None:
		self.strips = []	# Dereference the original array. Garbage collector will delete images on its own.
		return None
	
	# Takes the image strips and stitches them together to form a full background.
	# This method creates an ImagePair which contains the original (unsegmented) background image
	# and also the segmented image as one-hot encodings.
	# All images will be:
	#	224 rows
	#	340 columns 
	#	3 channels (BGR) for original images. *** RGB is incompatible with opencv ***
	#	7 or less channels for segmented images. *** one channel per one-hot encodings ***
	def virtual_stitch(self) -> ImageTuple:
		# 1.) Choose a random origin point 
		# The origin is the upper left corner of the image. 
		# This coordinate should exist in the strip
		# row = random.randint(224)		# might be zero every time; random.randint(224)
		# strip_width = self.strips[0][3] - self.strips[0][2]
		# col = random.randint(0, strip_width - 340)	# something between 0 and (strip width - 340)
		# origin = (row, col)

		# 2.) Size is always the same 
		size = globals.IMAGE_SIZE_2D		# height and width

		# 3.) Final images
		global_row = 0
		for strip in self.strips:
			global_row += strip.shape[0]

		# --- Original ---
		# Create blank image for background pixels
		original = np.zeros((global_row, globals.IMAGE_SIZE_3D[1], 3), dtype=np.uint8)			# BGR image:
		
		# --- Segmentation ---
		# Create labeled image for segmented pixels. All pixels are labeled as BG pixels.
		segmented = np.zeros(globals.IMAGE_SIZE_2D, dtype=np.uint8) + SegmentationLabels.BACKGROUND1
		
		# --- Mask (all 255) ---
		# Create image mask. All pixels are "foreground" pixels. Meaning not transparent.
		mask = np.zeros(size, dtype=np.uint8) + 255

		# --- Inverse Mask (all 0) ---
		# Create inverse mask.
		inv_mask = np.zeros(size, dtype=np.uint8)

		# 4.) --- Fill Original Image ---
		# Crop each parallax strip and stitch it to the final image
		
		# Make sure parallax strips have been loaded
		if len(self.strips) == 0:
			print("Error: background.py: Background image has not been loaded. Make sure to call load() method")
		
		row_counter = 0
		for strip in self.strips:
			left_col = random.randint(0, strip.shape[1] - globals.IMAGE_SIZE_2D[1] - 1)
			difference = strip.shape[0]
			original[row_counter : row_counter + difference, 0 : globals.IMAGE_SIZE_2D[1]] = strip[:, left_col : left_col + globals.IMAGE_SIZE_2D[1]]
			row_counter += difference
		
		row_end = original.shape[0] - globals.IMAGE_SIZE_2D[0]
		row_end = max(row_end, 1)	# make sure bottom row is greater than 0
		row = random.randint(0, row_end)

		original = original[row:row+224,:,:]

		# Coordinates are stored in units of pixels in the form: 
		#	[ top row,   bottom row, left column, right column ]
		#	[ inclusive, exclusive,  inclusive,   exclusive	]
		
		# *** Now we have our original image stitched ***

		# 6.) Package the images together
		img_tuple = ImageTuple(original, segmented, inv_mask, mask)
		
		return img_tuple

# ------------------------------------- GREEN HILL ZONE -----------------------

class GreenHillBackground(BackgroundBase):
	# --- Methods ---

	# Loads image from png file and stores in strips
	def virtual_load(self) -> None:
		# 1.) Load image
		img = cv.imread(ImageFileLookup.BackgroundLookup.green_hill_zone) # See ImageFileLookup for actual path

		# 2.) Crop image
		coords = ImageCropLookup.BackgroundLookup.green_hill_zone

		# 3.) Store in self.strips array
		for coord in coords: # get rows and columns from ImageCropLookup
			self.strips.append(img[coord[0]:coord[1], coord[2]:coord[3]])

# ------------------------------------- LABYRINTH ZONE ------------------------

class LabyrinthBackground(BackgroundBase):
	# --- Methods ---
	def virtual_load(self) -> None:
		# 1.) Load image
		img = cv.imread(ImageFileLookup.BackgroundLookup.labyrinth_zone)

		# 2.) Crop image
		coords = ImageCropLookup.BackgroundLookup.labyrinth_zone

		# 3.) Store in self.strips array
		for coord in coords:
			self.strips.append(img[coord[0]:coord[1], coord[2]:coord[3]])

# ------------------------------------- MARBLE ZONE ---------------------------

class MarbleBackgroundIndoors(BackgroundBase):
	# --- Methods ---
	def virtual_load(self) -> None:
		# 1.) Load image
		img1 = cv.imread(ImageFileLookup.BackgroundLookup.marble_zone_indoors_act1)
		img2 = cv.imread(ImageFileLookup.BackgroundLookup.marble_zone_indoors_act2)
		img3 = cv.imread(ImageFileLookup.BackgroundLookup.marble_zone_indoors_act3)

		# 2.) Crop image
		coords1 = ImageCropLookup.BackgroundLookup.marble_zone_indoors_act1
		coords2 = ImageCropLookup.BackgroundLookup.marble_zone_indoors_act2
		coords3 = ImageCropLookup.BackgroundLookup.marble_zone_indoors_act3

		# 3.) Store in self.strips array
		for coord in coords1:
			self.strips.append(img1[coord[0]:coord[1], coord[2]:coord[3]])
		for coord in coords2:
			self.strips.append(img2[coord[0]:coord[1], coord[2]:coord[3]])
		for coord in coords3:
			self.strips.append(img3[coord[0]:coord[1], coord[2]:coord[3]])

class MarbleBackgroundOutdoors(BackgroundBase):
	def virtual_load(self) -> None:
		img4 = cv.imread(ImageFileLookup.BackgroundLookup.marble_zone_outdoors)
		coords4 = ImageCropLookup.BackgroundLookup.marble_zone_outdoors
		for coord in coords4:
			self.strips.append(img4[coord[0]:coord[1], coord[2]:coord[3]])


# ------------------------------------- SPRING YARD ZONE ----------------------

class SpringYardBackground(BackgroundBase):
	# --- Methods ---
	def virtual_load(self) -> None:
		# 1.) Load image
		img = cv.imread(ImageFileLookup.BackgroundLookup.spring_yard_zone)

		# 2.) Crop image
		coords = ImageCropLookup.BackgroundLookup.spring_yard_zone

		# 3.) Store in self.strips array
		for coord in coords:
			self.strips.append(img[coord[0]:coord[1], coord[2]:coord[3]])

# ------------------------------------- STAR LIGHT ZONE -----------------------

class StarLightBackground(BackgroundBase):
	# --- Methods ---
	def virtual_load(self) -> None:
		# 1.) Load image
		img = cv.imread(ImageFileLookup.BackgroundLookup.star_light_zone)

		# 2.) Crop image
		coords = ImageCropLookup.BackgroundLookup.star_light_zone

		# 3.) Store in self.strips array
		for coord in coords:
			self.strips.append(img[coord[0]:coord[1], coord[2]:coord[3]])

# ------------------------------------- SCRAP BRAIN ZONE ----------------------

class ScrapBrainZoneAct1Background(BackgroundBase):
	# --- Methods ---
	def virtual_load(self) -> None:
		# 1.) Load image
		img = cv.imread(ImageFileLookup.BackgroundLookup.scrap_brain_zone_act1)

		# 2.) Crop image
		coords = ImageCropLookup.BackgroundLookup.scrap_brain_act1

		# 3.) Store in self.strips array
		for coord in coords:
			self.strips.append(img[coord[0]:coord[1], coord[2]:coord[3]])

class ScrapBrainZoneAct2Background(BackgroundBase):
	def virtual_load(self) -> None:
		img = cv.imread(ImageFileLookup.BackgroundLookup.scrap_brain_zone_act2)
		
		coords = ImageCropLookup.BackgroundLookup.scrap_brain_act2
		
		for coord in coords:
			self.strips.append(img[coord[0]:coord[1], coord[2]:coord[3]])

class ScrapBrainZoneDryBackground(BackgroundBase):
	def virtual_load(self) -> None:
		img = cv.imread(ImageFileLookup.BackgroundLookup.scrap_brain_zone_act3)
		
		coords = ImageCropLookup.BackgroundLookup.scrap_brain_act3_dry
		
		for coord in coords:
			self.strips.append(img[coord[0]:coord[1], coord[2]:coord[3]])

class ScrapBrainZoneWetBackground(BackgroundBase):
	def virtual_load(self) -> None:
		img = cv.imread(ImageFileLookup.BackgroundLookup.scrap_brain_zone_act3)
		
		coords = ImageCropLookup.BackgroundLookup.scrap_brain_act3_wet
		
		for coord in coords:
			self.strips.append(img[coord[0]:coord[1], coord[2]:coord[3]])

class Background:
	# Saves an array of all Background objects. 
	# Before calling stitch to create background images, make sure to 
	# call load() to load the strips into the objects memory. 
	# Not calling load() will result in empty images or an error.
	# When finished with a Background object, optionally call unload() to save memory.
	all_backgrounds = [
		GreenHillBackground(),
		LabyrinthBackground(),
		MarbleBackgroundIndoors(),
		MarbleBackgroundOutdoors(),
		SpringYardBackground(),
		StarLightBackground(),
		ScrapBrainZoneAct1Background(),
		ScrapBrainZoneAct2Background(),
		ScrapBrainZoneDryBackground(),
		ScrapBrainZoneWetBackground(),
	]

	def select_random():
		index = random.randint(0, len(Background.all_backgrounds) - 1)

		return Background.select(index)

	def select(index):
		return Background.all_backgrounds[index]