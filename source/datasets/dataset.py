import sys
import os

script_dir = os.path.dirname(os.path.abspath(__file__))
project_dir = os.path.abspath(script_dir + "/../..")

# Represents a training dataset which can be used for supervised machine learning.
class Dataset:
	None

	