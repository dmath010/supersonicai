import numpy as np
import random
import cv2 as cv
import sys
import os

script_dir = os.path.dirname(os.path.abspath(os.path.abspath(__file__)))	# absolute directory of current script's directory
project_dir = os.path.abspath(script_dir + "/../..")						# absolute directory of supersonicai 

sys.path.append(os.path.abspath(project_dir + '/source'))
sys.path.append(os.path.abspath(project_dir + '/source/vision'))

from globals import * 
from image_processing import * 
from image_file_lookup import ImageFileLookup
from image_crop_lookup import ImageCropLookup
from image_tuple import ImageTuple
from file_pair import FilePair
from color import Color
from segmentation_labels import SegmentationLabels

class Tilesets:
	class Base:
		def __init__(self):
			# array of ImageTuples. See class ImageTuple
			self.tiles = []
		
		# Creates an ImagePair.
		def stitch(self) -> ImageTuple:
			
			# --- Pick 2 random tiles ---
			leftIndex = random.randint(0, len(self.tiles) - 1)
			rightIndex = random.randint(0, len(self.tiles) - 1)
			
			# --- Grab the two images of the tilesets ---
			leftTileOriginal = self.tiles[leftIndex].original
			rightTileOriginal = self.tiles[rightIndex].original
			
			# --- Grab the 2 tileset segmentations also ---
			leftTileSegmented  = self.tiles[leftIndex].segmented
			rightTileSegmented = self.tiles[rightIndex].segmented 
			
			# --- Which pixels should be ignored (transparent background pixels) ---
			leftTileBG = self.tiles[leftIndex].background_mask
			rightTileBG = self.tiles[rightIndex].background_mask
			
			# --- Which pixels should we keep (solid foreground pixels) ---
			leftTileFG = self.tiles[leftIndex].foreground_mask
			rightTileFG = self.tiles[rightIndex].foreground_mask
			
			# --- Calculate total size ---
			
			height = leftTileOriginal.shape[0]
			width = rightTileOriginal.shape[1]
			
			# --- Stich both tiles together ---
			original = np.zeros((height, width + width, 3), dtype=np.uint8)
			original[:, 0:width] = leftTileOriginal
			original[:, width:width + width] = rightTileOriginal
			
			# --- Stich the segmentations in the same way ---
			segmented = np.zeros((height, width + width), dtype=np.uint8)
			segmented[:, 0:width] = leftTileSegmented
			segmented[:, width:width + width] = rightTileSegmented
			
			# --- Stich background mask ---
			bg_mask = np.zeros((height, width + width), dtype=np.uint8)
			bg_mask[:, 0:width] = leftTileBG
			bg_mask[:, width:width + width] = rightTileBG

			# --- Stich foreground mask ---
			fg_mask = np.zeros((height, width + width), dtype=np.uint8)
			fg_mask[:, 0:width] = leftTileFG
			fg_mask[:, width:width + width] = rightTileFG

			# *** Now we have an a set of oversized images: ***
			# *** original, segmented, foreground mask and background mask ***
			# *** We need to make it smaller ***

			# --- Crop image to the correct size ---
			top_row = random.randint(0, original.shape[0] - globals.IMAGE_SIZE_2D[0] - 1)
			lef_col = random.randint(0, original.shape[1] - globals.IMAGE_SIZE_2D[1] - 1)

			bot_row = top_row + globals.IMAGE_SIZE_2D[0]
			rig_col = lef_col + globals.IMAGE_SIZE_2D[1]

			original = original[top_row:bot_row, lef_col:rig_col]
			segmented = segmented[top_row:bot_row, lef_col:rig_col]
			fg_mask = fg_mask[top_row:bot_row, lef_col:rig_col]
			bg_mask = bg_mask[top_row:bot_row, lef_col:rig_col]
			
			return ImageTuple(original, segmented, fg_mask, bg_mask)
		
		# Creates the full mask over the entire original and segmented image containing all the tiles.
		# This method picks out which pixels are part of the background and which are part of the 
		# foreground based off of the segmented image only (not the original). 
		# These masks can be used to stitch the tilesets with other parts of the game image.
		# `original`	-	Original image.
		# `bg_color`	-	Color of all background pixels. (Stuff we can walk through)
		# `fg_color`	-	Color of all foreground pixels. (Stuff we can walk on)
		# 
		# returns		-	Tuple of 2 np.ndarrays. 
		#					first:	background mask
		#					second:	foreground mask
		def __create_masks__(self, original:np.ndarray, color:Color):
			
			bg_mask = mask_by_color(original, Color)
			fg_mask = cv.bitwise_not(bg_mask) 

			return (bg_mask, fg_mask)

		# Label pixels of a segmented image with integer labels.
		# Pixels label is determined by the shade of color of the cooresponding
		#	pixel in the manualy segmented image.
		# `segmented`	manully segmented image (RGB)
		# `bg1_color`	-	Color of all background 1 pixels.	(Stuff we can only look at)
		# `bg2_color`	-	Color of all background 2 pixels.	(Stuff we can walk through)
		# `sg_color`	-	Color of all stage pixels.			(Stuff we can walk on)
		# `mc_color`	-	Color of all mechanical pixels.		(Stuff we can interact with)
		# `shades`		-	Array of tuples
		#						1st - shade of color from manually segmented image
		#						2nd - segmentation label cooresponding to that color.
		def __label_segmented__(self, 
			segmented:np.ndarray,
			shades) -> np.ndarray:
			
			rows = segmented.shape[0]
			cols = segmented.shape[1]

			labels = np.zeros((rows, cols), dtype=np.uint8)

			for shade in shades:
				color = shade[0]
				segmentation_label = shade[1]

				# Determine which pixels are from which segment.
				# Store as binary images (false = 0, true = 255)
				mask = mask_by_color(segmented, color)

				# Set these pixels with their cooresponding integer labels
				labeled_pixels = cv.bitwise_and(mask, segmentation_label)
				
				# Combine labels into one image
				labels = overlay_images(labels, labeled_pixels, mask)

			return labels
			
		# `image_filepath`	- absolute path to the original and segmented image to be loaded.
		# `crops`			- coordinates of each section (subimage) of both the original and segmented images
		# `canvas_color`	- color of the background pixels of the original (unsegmented) image
		#						This identifies which pixels are to be transparent in both 
		#						the original and segmented images.
		# `shades`		-	Array of tuples
		#						1st - shade of color from manually segmented image
		#						2nd - segmentation label cooresponding to that color.
		def __load_helper__(
			self, 
			image_filepair:FilePair, 
			crops, 
			canvas_color:Color,
			shades):
			original = cv.imread(image_filepair.original)
			segmented = cv.imread(image_filepair.segmentation)
			
			if original is None:
				print("Error:", image_filepair.original, "could not be opened")
				return

			if segmented is None:
				print("Error:", image_filepair.segmentation, "could not be opened")
				return 

			canvas_mask = mask_by_color(original, canvas_color)
			fg_mask = cv.bitwise_not(canvas_mask)
			
			segmented = self.__label_segmented__(segmented, shades)
			
			for crop in crops:
				tile_original = original[crop[0]:crop[1], crop[2]:crop[3]]
				
				tile_segmented = segmented[crop[0]:crop[1], crop[2]:crop[3]]
				
				# TODO: tile_segmented = segmented[crop[0]:crop[1], crop[2]:crop[3]]
				tile_bg_mask = canvas_mask[crop[0]:crop[1], crop[2]:crop[3]]
				tile_fg_mask = fg_mask[crop[0]:crop[1], crop[2]:crop[3]]
				
				image_tuple = ImageTuple(
					original=tile_original, 
					segmented=tile_segmented, 
					background_mask=tile_bg_mask,
					foreground_mask=tile_fg_mask)
				
				self.tiles.append(image_tuple)
		
		def unload(self) -> None:
			self.tiles = []	# dereference array. Garbage collected will delete images on its own.
			return 

	class GreenHillZone(Base):
		def load(self):
			shades = [
				( Color(0, 0, 0),		SegmentationLabels.BACKGROUND1 ),
				( Color(255, 255, 255), SegmentationLabels.BACKGROUND2 ),
				( Color(11, 80, 14),	SegmentationLabels.STAGE ),
				( Color(11, 11, 11),	SegmentationLabels.MECHANICAL ),
			]
			
			self.__load_helper__(
				ImageFileLookup.TilesetsLookup.green_hill_zone,
				ImageCropLookup.TilesetsLookup.green_hill_zone,
				Color(135, 16, 19),
				shades,
			)
			
	class StarLightZone(Base):	
		def load(self):
			shades = []
			
			self.__load_helper__(
				ImageFileLookup.TilesetsLookup.star_light_zone,
				ImageCropLookup.TilesetsLookup.star_light_zone,
				Color(223, 100, 128),
				shades,
			)

	class LabyrinthZoneDry(Base):
		def load(self):
			shades = [
				( Color(38, 5, 60),		SegmentationLabels.BACKGROUND1 ),
				( Color(0, 255, 227),	SegmentationLabels.BACKGROUND2 ),
				( Color(241, 255, 2),	SegmentationLabels.STAGE ),
				( Color(255, 152, 0),	SegmentationLabels.MECHANICAL ),
			]
			
			self.__load_helper__(
				ImageFileLookup.TilesetsLookup.labyrinth_zone_dry,
				ImageCropLookup.TilesetsLookup.labyrinth_zone,
				Color(38, 5, 60),
				shades,
			)

	class LabyrinthZoneWet(Base):
		def load(self):
			shades = [
				( Color(38, 5, 60),		SegmentationLabels.BACKGROUND1 ),
				( Color(0, 255, 227),	SegmentationLabels.BACKGROUND2 ),
				( Color(241, 255, 2),	SegmentationLabels.STAGE ),
				( Color(255, 152, 0),	SegmentationLabels.MECHANICAL ),
			]
			
			self.__load_helper__(
				ImageFileLookup.TilesetsLookup.labyrinth_zone_wet,
				ImageCropLookup.TilesetsLookup.labyrinth_zone,
				Color(38, 5, 60),
				shades,
			)

	class MarbleZone(Base):
		def load(self):
			shades = [
				( Color(0, 0, 0),		SegmentationLabels.BACKGROUND1 ),
				( Color(72, 36, 108),	SegmentationLabels.BACKGROUND2 ),
				( Color(43, 144, 47),	SegmentationLabels.STAGE ),
				( Color(255, 126, 0),	SegmentationLabels.HAZARDS ),
			]
			
			self.__load_helper__(
				ImageFileLookup.TilesetsLookup.marble_zone,
				ImageCropLookup.TilesetsLookup.marble_zone,
				Color(75, 75, 75),
				shades,
			)

	class ScrapBrainZoneAct12(Base):
		def load(self):
			shades = []

			self.__load_helper__(
				ImageFileLookup.TilesetsLookup.scrap_brain_zone_act12,
				ImageCropLookup.TilesetsLookup.scrap_brain_zone_act12,
				Color(135, 16, 19),
				shades,
			)
	
	class ScrapBrainZoneAct3Dry(Base):
		def load(self):
			shades = []
			
			self.__load_helper__(
				ImageFileLookup.TilesetsLookup.scrap_brain_zone_act3_dry,
				ImageCropLookup.TilesetsLookup.scrap_brain_zone_act3_dry,
				Color(135, 16, 19),
			)

	class ScrapBrainZoneAct3Wet(Base):
		def load(self):
			shades = []
			
			self.__load_helper__(
				ImageFileLookup.TilesetsLookup.scrap_brain_zone_act3_wet,
				ImageCropLookup.TilesetsLookup.scrap_brain_zone_act3_wet,
				Color(135, 16, 19),
			)

	class SpringYardZone(Base):
		def load(self):
			# TODO: fix manually segmented image and add more labels
			shades = [
				( Color(0, 0, 0), SegmentationLabels.BACKGROUND1 ),
				( Color(255, 255, 255),	SegmentationLabels.STAGE ),
			]

			self.__load_helper__(
				ImageFileLookup.TilesetsLookup.spring_yard_zone,
				ImageCropLookup.TilesetsLookup.spring_yard_zone,
				Color(131, 76, 158),
				shades,
			)
	
	all_tileset = [
		GreenHillZone(),
		MarbleZone(),
		LabyrinthZoneDry(),
		LabyrinthZoneWet(),
		#ScrapBrainZoneAct12(),
		#ScrapBrainZoneAct3Dry(),
		#ScrapBrainZoneAct3Wet(),
		SpringYardZone(),
		#StarLightZone(),
	]

	def select_random():
		index = random.randint(0, len(Tilesets.all_tileset) - 1)

		return Tilesets.select(index)

	def select(index):
		return Tilesets.all_tileset[index]
