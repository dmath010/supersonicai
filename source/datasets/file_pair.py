import os

class FilePair:
	# dir			- absolute path to directory where both the original and segmented files are stored (can end in '/')
	# original		- path to original file relative to 'dir'
	# segmentation	- path to segmentation file relative to 'dir'
	def __init__(self, dir, original, segmentation):
		dir = os.path.abspath(dir)
		self.original = os.path.abspath(dir + '/' + original)
		self.segmentation = os.path.abspath(dir + '/' + segmentation)
	
	def __str__(self) -> str:
		return \
			"original:     \'" + self.original + "\'\n" \
			"segmentation: \'" + self.segmentation + "\'\n" 
	