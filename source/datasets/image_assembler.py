import numpy as np
import cv2 as cv
import sys
import os

script_dir = os.path.dirname(os.path.abspath(os.path.abspath(__file__)))	# absolute directory of current script's directory
project_dir = os.path.abspath(script_dir + "/../..")						# absolute directory of supersonicai 

sys.path.append(os.path.abspath(project_dir + '/source/dataset'))	
sys.path.append(os.path.abspath(project_dir + '/source'))

from globals import * 
from image_file_lookup import *

# Creates a single image out of multiple game maps and sprites
class ImageAssembler:
	def __init__(self):
		self.image = np.zeros(shape=globals.IMAGE_SIZE_3D, dtype=np.uint8)

	def assemble_demo(self):
		# 0.) Create a blank image
		self.image = np.zeros(shape=[224, 340, 3], dtype=np.uint8)
		
		for row in range(0, self.image.shape[0]):
			for col in range(0, self.image.shape[1]):
				self.image[row, col] = row & col

		# 1.) Load a background image
		img_dir = os.path.join(project_dir, "data/SpritersResource")
		lookup = ImageFileLookup(img_dir)
		spring_yard_zone = cv.imread(lookup.backgrounds.spring_yard_zone)

		#cv.imshow("spring_yard_zone", spring_yard_zone)
		#cv.waitKey(0)
		#cv.destroyAllWindows()

		# 2.) Crop parts of spring yard zone
		# upper left corner (row, col)
		origin1 = (448, 24)
		origin2 = (536, 24)
		origin3 = (640, 24)

		# lower right corner (row, col)
		point1 = (527, 340)
		point2 = (631, 340)
		point3 = (655, 340)

		# (n rows, n cols)	or	(height, width)
		size1 = (point1[0] - origin1[0], self.image.shape[1])
		size2 = (point2[0] - origin2[0], self.image.shape[1])
		size3 = (point3[0] - origin3[0], self.image.shape[1])

		background1 = spring_yard_zone[origin1[0]:origin1[0] + size1[0], origin1[1]:origin1[1] + size1[1]]
		background2 = spring_yard_zone[origin2[0]:origin2[0] + size2[0], origin2[1]:origin2[1] + size2[1]]
		background3 = spring_yard_zone[origin3[0]:origin3[0] + size3[0], origin3[1]:origin3[1] + size3[1]]
		
		#shape1 = background1.shape
		#shape2 = background2.shape
		#shape3 = background3.shape

		#cv.imshow("bg1", background1)
		#cv.imshow("bg2", background2)
		#cv.imshow("bg3", background3)
		#cv.waitKey(0)
		#cv.destroyAllWindows()

		# 3.) Copy Background onto the blank image

		row = 0
		self.image[row:row + size1[0],0:size1[1]] = background1
		row += size1[0]
		self.image[row:row + size2[0],0:size2[1]] = background2
		row += size2[0]
		self.image[row:row + size3[0],0:size3[1]] = background3
		row += size3[0]

		#cv.imshow("spring yard zone", self.image)
		#cv.waitKey(0)
		#cv.destroyAllWindows()

		# 4.) Copy Stage onto image
		full_stage = cv.imread(lookup.stages.spring_yard_act1.original, cv.IMREAD_COLOR)
		stage = full_stage[1050:1050+self.image.shape[0], 2860:2860+self.image.shape[1]]
		
		added = cv.add(self.image, stage)
		#cv.imshow("added", added)
		#cv.waitKey(0)
		#cv.destroyAllWindows()

		# stage contains pixels which need to be transparent
		#stage_hsv = cv.cvtColor(stage, cv.COLOR_BGR2HSV)
		#stage_hue = stage_hsv[:, :, 0]	# hue channel only 
		stage_red = stage[:, :, 2]		# red channel only
		stage_green = stage[:, :, 1]	# green channel only
		stage_blue = stage[:, :, 0]		# blue channel only

		_, mask_red_lower = cv.threshold(stage_red, 130, 255, cv.THRESH_BINARY)			# Set 1's for all pixels which are less than purple
		_, mask_red_upper = cv.threshold(stage_red, 135, 255, cv.THRESH_BINARY_INV)		# Set 1's for all pixels which are less than purple
		mask_bot = cv.bitwise_and(mask_red_lower, mask_red_upper)						# Set 1's for all pixels which are purple
		mask_top = cv.bitwise_not(mask_bot)												# Set 0's for all pixels which are purple

		stage_pixels = cv.bitwise_and(stage, stage, mask=mask_top)						# cut silouette of stage
		self.image = cv.bitwise_and(self.image, self.image, mask=mask_bot)				# cut silouette of stage
		#cv.imshow("bg sillouette", self.image)
		self.image = cv.add(self.image, stage_pixels)									# combine silouettes

		#cv.imshow("mask_top", mask_top)
		#cv.imshow("mask_bot", mask_bot)
		#cv.imshow("stage_sillouette", stage_pixels)
		#cv.imshow("stage_red", stage_red)
		#cv.imshow("mask_red_lower", mask_red_lower)
		#cv.imshow("mask_red_upper", mask_red_upper)
		#cv.imshow("stage", stage)
		#cv.imshow("image", self.image)
		#cv.waitKey(0)
		#cv.destroyAllWindows()

		# ----------------- SPRITES -----------------------

		# Load image from file
		objects_common = cv.imread(lookup.stage_objects.common_objects.original)
		cv.imshow("objects_common", objects_common)

		# Crop a ring
		origin = (198, 24)
		point = (213, 40)

		#self.image[row:row + size2[0],0:size2[1]] 
		ring = objects_common[origin[0]:point[0], origin[1]:point[1]]
		cv.imshow("ring", ring)

		ring_red = ring[:, :, 2]
		ring_green = ring[:, :, 1]
		ring_blue = ring[:, :, 0]

		_, ring_mask_lower = cv.threshold(ring_green, 75, 255, cv.THRESH_BINARY_INV)
		cv.imshow("ring_mask_lower", ring_mask_lower)

		ring_mask_not = cv.bitwise_not(ring_mask_lower)
		cv.imshow("ring_mask_not", ring_mask_not)

		ring_sillouette_neg = cv.bitwise_and(ring, ring, mask=ring_mask_lower)
		cv.imshow("ring_sillouette", ring_sillouette_neg)
		
		cv.waitKey(0)
		cv.destroyAllWindows()

	def sprite_updating(self):
		badniks = cv.imread(ImageFileLookup.SpritesFileLookup.badniks.original)
		cv.imshow("robots", badniks)
		cv.waitKey(0)
		cv.destroyAllWindows()
		robot = badniks[200:238, 25:72]
		ring_red = robot[:, :, 2]
		ring_green = robot[:, :, 1]
		ring_blue = robot[:, :, 0]
		_, mask1 = cv.threshold(ring_red, 15, 255, cv.THRESH_BINARY)
		_, mask2 = cv.threshold(ring_green, 75, 255, cv.THRESH_BINARY)
		_, mask3 = cv.threshold(ring_blue, 9, 255, cv.THRESH_BINARY)
		_, mask4 = cv.threshold(ring_red, 11, 255, cv.THRESH_BINARY)
		_, mask5 = cv.threshold(ring_green, 70, 255, cv.THRESH_BINARY)
		_, mask6 = cv.threshold(ring_blue, 5, 255, cv.THRESH_BINARY)
		mask_red = cv.bitwise_xor(mask1, mask4)
		mask_green = cv.bitwise_xor(mask2, mask5)
		mask_blue = cv.bitwise_xor(mask3, mask6)
		mask = cv.bitwise_or(mask_red, mask_green)
		mask = cv.bitwise_or(mask, mask_blue)
		mask = cv.bitwise_not(mask)
		cutout = cv.bitwise_and(robot, robot, mask=mask)
		cv.imshow("robots", robot)
		cv.imshow("mask", mask)
		cv.imshow("cutout", cutout)
		cv.waitKey(0)
		cv.destroyAllWindows()
	
