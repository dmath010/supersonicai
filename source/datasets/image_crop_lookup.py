
# Stores coordinates of sprites reletive to their respective images.
# 
# Coordinates are stored in units of pixels in the form: 
#	[ top row,   bottom row, left column, right column ]
#	[ inclusive, exclusive,  inclusive,   exclusive    ]
#	
# Coordinates apply to both versions of the images (segmented and unsegemented)
# 
# Organized under namespaces classes
# These classes are not ment to be instantiated.
# 
class ImageCropLookup:

	# --------------------------------- Small Sprites -------------------------
	
	class SpritesLookup:
		# --- Sonic ---
		# TODO: swap from 2 3 0 1 to 0 1 2 3
		#		Don't forget to fix indexing in SonicSprites.load()
		sonic = [
			[ 257, 298, 42, 76 ],
			[ 253, 298, 127, 164 ],
			[ 256, 296, 199, 233 ],
			[ 257, 298, 267, 303 ],
			[ 257, 298, 337, 373 ],
			[ 257, 297, 423, 459 ],
			[ 264, 299, 506, 548 ],
			
			###[ 252, 303, 296, 623 ],
			[ 349, 391, 44, 71 ],
			[ 345, 389, 107, 151 ],
			[ 347, 389, 177, 211 ],
			[ 348, 389, 247, 290 ],
			[ 348, 389, 317, 361 ],
			[ 348, 389, 387, 433 ],
			[ 348, 389, 476, 514 ],
			[ 348, 389, 539, 583 ],
			
			[ 437, 481, 37, 80 ],
			[ 437, 487, 106, 149 ],
			[ 438, 481, 178, 222 ],
			[ 438, 480, 248, 291 ],
			[ 436, 488, 316, 361 ],
			[ 437, 481, 387, 440 ],
			[ 437, 480, 462, 505 ],
			[ 436, 481, 531, 575 ],
			
			[ 528, 573, 37, 71 ],
			[ 531, 573, 108, 141 ],
			[ 531, 573, 177, 212 ], 
			[ 531, 573, 247, 282 ],
			[ 531, 573, 335, 379 ],
			[ 531, 573, 405, 449 ],
			[ 531, 573, 475, 519 ],
			###[ 531, 573, 545, 573 ],
			
			[ 622, 659, 41, 77 ],
			[ 622, 659, 111, 146 ],
			[ 622, 659, 181, 215 ],
			[ 622, 659, 251, 287 ],
			[ 622, 659, 322, 355 ],
			[ 622, 663, 402, 438 ],
			[ 619, 661, 481, 507 ],
			[ 620, 663, 542, 578 ],
			[ 619, 662, 620, 647 ],
			
			[ 712, 750, 33, 84 ],
			[ 714, 750, 101, 155 ],
			[ 718, 746, 193, 245 ],
			[ 718, 746, 260, 310 ],
			[ 718, 746, 326, 377 ],
			[ 718, 746, 395, 439 ],
			###[ 718, 746, 482, 746 ],
			[ 709, 752, 559, 602 ],
			
			[ 804, 842, 37, 81 ],
			[ 804, 842, 107, 151 ],
			[ 796, 848, 192, 237 ],
			[ 796, 848, 279, 322 ],
			[ 817, 850, 366, 407 ],
			[ 816, 852, 435, 479 ],
			#505, / 548,852				# TODO: 
			###[ 828, 852, 571, 623 ],
			
			[ 929, 970, 44, 76 ],
			[ 927, 971, 109, 144 ],
			[ 927, 971, 195, 229 ], 
			[ 927, 971, 265, 300 ], 
			[ 927, 971, 334, 370 ],
			[ 927, 971, 404, 439 ],
			[ 927, 971, 474, 509 ],
			[ 927, 971, 544, 581 ],
			
			[ 1055, 1130, 63, 114 ],
			
			[ 1221, 1250, 37, 81 ],
			[ 1218, 1254, 110, 146 ],
			[ 1214, 1258, 185, 213 ],
			[ 1217, 1253, 251, 287 ],
			[ 1210, 1261, 334, 377 ],
			[ 1213, 1259, 422, 458 ],
			[ 1206, 1257, 509, 538 ],
			
			[ 1313, 1339, 37, 87 ],
			[ 1313, 1341, 107, 158 ],
			[ 1305, 1351, 197, 232 ],
			[ 1304, 1349, 267, 303 ],
			[ 1309, 1346, 341, 370 ],
			[ 1313, 1340, 415, 435 ],
			[ 1317, 1337, 482, 502 ],
						
			[ 1392, 1442, 37, 79 ],
			[ 1391, 1443, 106, 148 ],
		]
			
		# --- Eggman ---
		eggman = [
				
		]
		
		# --- Badniks ---
		badniks = [
			# crabmeat / ganignani
			[ 200, 238, 25, 72 ],	# crab1
			[ 200, 238, 79, 130 ],	# crab2
			[ 197, 240, 136, 185 ],	# crab3
			[ 197, 240, 192, 240 ],	# crab4
			[ 200, 238, 246, 297 ],	# crab5
			[ 200, 214, 305, 322 ],	# crab6
			[ 216, 240, 305, 322 ],	# crab7
			
			# Buzz Bomber / beeron
			[ 278, 303, 23, 72 ],	# crab1
			[ 274, 310, 79, 120 ],	# crab2
			[ 274, 292, 135, 153 ],	# crab3
			[ 298, 316, 135, 153 ],	# crab4
			###[ 330, 340, 28, 67 ],	# crab5
			[ 346, 379, 23, 72 ],	# crab6
			[ 386, 411, 23, 72 ],	# crab7
			[ 346, 379, 79, 128 ],	# crab8
			#[ 386, 379, 411, 128 ],	# crab9
			
			[ 421, 472, 23, 80 ],	# crab10
			[ 418, 476, 87, 144 ],	# crab11
			###[ 414, 423, 27, 67 ],	# crab12
			###[ 414, 424, 79, 90 ],	# crab13
			[ 313, 324, 95, 113 ],	# crab14
			[ 323, 339, 135, 153 ],	# crab15
			
			#Motobug / Motora
			[ 274, 308, 173 ,222 ],
			[ 274, 307, 228, 278 ],
			###[ 274, 283, 284, 293 ],	# dust
			[ 313, 348, 172, 223 ],
			[ 313, 347, 228, 278 ],
			###[ 290, 300, 284, 294 ],		# dust
			###[ 305, 317, 283, 295 ],		# dust
			
			# Masher / Batabata
			[ 381, 415, 172, 205 ],
			#[ 212, 381, 414, 245 ],
			
			# Blue Newtron / Meleon
			[ 521, 564, 22, 66 ],
			[ 521, 564, 70, 115 ],
			[ 521, 565, 118, 163 ],
			[ 569, 612, 22, 66 ],
			[ 569, 597, 70, 115 ],
			[ 569, 589, 118, 162 ],
			[ 593, 605, 122, 134 ],
			[ 593, 604, 138, 157 ],
			
			# Bomb
			[ 791, 827, 22, 49 ],
			[ 791, 827, 55, 81 ],
			[ 792, 827, 87, 114 ],
			[ 791, 827, 118, 146 ],
			[ 791, 828, 151, 178 ],
			[ 831, 867, 22, 51 ],
			[ 832, 867, 56, 81 ],
			[ 832, 867, 87, 113 ],
			[ 833, 867, 120, 145 ],
			[ 831, 867, 152, 178 ],
			###[ 872, 891, 23, 33 ],	# stick
			###[ 871, 891, 38, 51 ],
			###[ 879, 971, 70, 81 ],
			###[ 879, 891, 86, 99 ],
			###[ 879, 892, 103, 130 ],
			###[ 879, 892, 134, 163 ],
			[ 790, 837, 181, 211 ],
			[ 791, 837, 214, 242 ],
			[ 791, 837, 247, 275 ],
			[ 791, 837, 278, 306 ],
			[ 791, 838, 310, 338 ],
			[ 841, 888, 182, 210 ],
			[ 840, 887, 214, 242 ],
			[ 841, 887, 245, 275 ],
			[ 840, 888, 278, 306 ],
			[ 841, 888, 310, 338 ],
			###[ 894, 940, 21, 50 ],
			###[ 895, 939, 54, 83 ],
			###[ 895, 939, 86, 114 ],
			###[ 895, 940, 117, 146 ],
			###[ 895, 940, 150, 178 ],
			###[ 895, 940, 181, 210 ],
			###[ 895, 940, 214, 243 ],
			###[ 895, 940, 246, 275 ],
			###[ 899, 936, 278, 307 ],
			###[ 943, 997, 21, 51 ],
			###[ 942, 996, 54, 82 ],
			###[ 947, 992, 86, 115 ],
			###[ 947, 992, 119, 147 ],
			###[ 945, 992, 150, 181 ],
			###[ 947, 992, 181, 210 ],
			###[ 947, 992, 214, 243 ],
			###[ 947, 992, 246, 274 ],
			###[ 947, 992, 278, 307 ],
			
			# Caterkiller / Nal
			[ 196, 224, 358, 377 ],
			[ 196, 224, 381, 402 ],
			[ 200, 220, 406, 427 ],
			[ 244, 282, 358, 402 ],
			[ 245, 282, 406, 451 ],
			[ 302, 330, 358, 417 ],
			[ 302, 330, 422, 482 ],
			
			# Spikes / Yadorin
			###[ 374, 394, 359, 386 ],
			###[ 375, 396, 395, 425 ],
			[ 396, 442, 357, 403 ],
			[ 398, 442, 406, 451 ],
			[ 398, 442, 453, 499 ],
			[ 444, 489, 357, 403 ],
			[ 446, 490, 406, 450 ],
			[ 445, 489, 454, 499 ],
			[ 493, 537, 357, 403 ],
			[ 492, 538, 406, 452 ],
			[ 492, 536, 454, 499 ],
			
			# Batbrain / Basaran
			###[ 206, 235, 514, 535 ],	# afterburner
			###[ 206, 218, 537, 550 ],	# afterburner
			[ 223, 235, 538, 551 ],
			[ 238, 275, 513, 551 ],
			[ 243, 272, 553, 591 ],
			[ 238, 276, 594, 623 ],
			[ 278, 315, 513, 550 ],
			[ 282, 312, 554, 599 ],
			[ 278, 315, 602, 639 ],
			[ 318, 355, 514, 552 ],
			[ 322, 351, 553, 598 ],
			[ 319, 356, 602, 639 ],
			
			# Roller / Arma
			[ 788, 843, 513, 550 ],
			[ 798, 842, 554, 590 ],
			[ 805, 843, 593, 633 ],
			[ 845, 883, 514, 551 ],
			[ 844, 883, 554, 591 ],
			[ 845, 882, 594, 631 ],
			[ 901, 939, 513, 550 ],
			[ 901, 938, 554, 591 ],
			[ 901, 939, 594, 631 ],

			# Burrobot / Mogurin
			[ 212, 268, 687, 716 ],
			[ 217, 264, 717, 748 ],
			[ 213, 263, 750, 781 ],
			[ 265, 319, 653, 684 ],
			[ 266, 320, 684, 716 ],
			[ 265, 315, 716, 749 ],
			[ 267, 315, 751, 783 ],
			[ 319, 362, 653, 688 ],
			[ 319, 363, 688, 723 ],
			[ 332, 363, 731, 777 ],
			[ 362, 406, 653, 688 ],
			[ 318, 364, 687, 726 ],
			[ 330, 364, 731, 778 ],
			[ 364, 409, 652, 689 ],
			[ 364, 407, 689, 724 ],
					
			# Orbinaut / Unidus 
			[ 795, 846, 197, 246 ],
			[ 851, 903, 197, 246 ],
			[ 907, 959, 197, 246 ],
			[ 966, 1014, 197, 246 ],
			[ 1020, 1070, 197, 246 ],
			[ 1077, 1126, 197, 246 ],
			# [ 1132, 1182, 197, 246 ],
			# [ 1187, 1239, 197, 246 ],
			# [ 1244, 1295, 197, 246 ],
			# [ 1300, 1350, 197, 246 ],
			# [ 1355, 1407, 197, 246 ],
			[ 797, 846, 254, 304 ],
			[ 853, 904, 254, 304 ],
			[ 909, 958, 254, 304 ],
			[ 965, 1015, 254, 304 ],
			[ 1021, 1071, 254, 304 ],
			[ 1076, 1126, 254, 304 ],
			# [ 1130, 1183, 254, 304 ],
			# [ 1188, 1239, 254, 304 ],
			# [ 1246, 1295, 254, 304 ],
			# [ 1299, 1354, 254, 304 ],
			# [ 1356, 1406, 254, 304 ],
			[ 797, 846, 311, 358 ],
			[ 850, 903, 311, 358 ],
			[ 909, 961, 311, 358 ],
			[ 965, 1015, 311, 358 ],
			[ 1021, 1071, 311, 358 ],
			[ 1076, 1128, 311, 358 ],
			# [ 1132, 1182, 311, 358 ],
			# [ 1188, 1239, 311, 358 ],
			# [ 1244, 1296, 311, 358 ],
			# [ 1302, 1351, 311, 358 ],
			# [ 1357, 1407, 311, 358 ],
			[ 800, 818, 369, 384 ],
			[ 827, 843, 368, 382 ],
			[ 796, 819, 396, 415 ],
			
			# Orbinaut / Unidus (Star Light Zone) 
			[ 797, 847, 451, 499 ],
			[ 852, 902, 451, 499 ],
			[ 909, 958, 451, 499 ],
			[ 966, 1015, 451, 499 ],
			[ 1021, 1071, 451, 499 ],
			[ 1079, 1129, 451, 499 ],
			# [ 1134, 1183, 451, 499 ],
			# [ 1191, 1238, 451, 499 ],
			# [ 1245, 1294, 451, 499 ],
			# [ 1301, 1350, 451, 499 ],
			# [ 1358, 1406, 451, 499 ],
			[ 799, 846, 509, 556 ],
			[ 856, 903, 509, 556 ],
			[ 909, 958, 509, 556 ],
			[ 968, 1015, 509, 556 ],
			[ 1023, 1071, 509, 556 ],
			[ 1080, 1124, 509, 556 ],
			# [ 1135, 1181, 509, 556 ],
			# [ 1191, 1238, 509, 556 ],
			# [ 1247, 1295, 509, 556 ],
			# [ 1302, 1352, 509, 556 ],
			# [ 1359, 1405, 509, 556 ],
			[ 799, 847, 565, 614 ],
			[ 856, 902, 565, 614 ],
			[ 910, 960, 565, 614 ],
			[ 967, 1015, 565, 614 ],
			[ 1023, 1072, 565, 614 ],
			[ 1079, 1127, 565, 614 ],
			# [ 1135, 1182, 565, 614 ],
			# [ 1191, 1240, 565, 614 ],
			# [ 1247, 1294, 565, 614 ],
			# [ 1302, 1351, 565, 614 ],
			# [ 1361, 1406, 565, 614 ],
			[ 796, 819, 617, 641 ],
			[ 824, 844, 617, 637 ],
			[ 796, 820, 644, 668 ],
			[ 823, 848, 645, 669 ],
			
			# Ball Hog / Ton-ton 
			[ 797, 823, 702, 746 ],
			[ 828, 854, 702, 746 ],
			[ 860, 887, 705, 740 ],
			[ 892, 919, 703, 746 ],
			[ 924, 943, 703, 723 ],
			[ 924, 943, 727, 746 ],
			# Prototype Ball Hog
			[ 793, 826, 783, 826 ],
			[ 833, 865, 784, 826 ],
			[ 872, 908, 783, 826 ],
			[ 912, 946, 782, 827 ],
			[ 796, 830, 831, 873 ],
			[ 876, 895, 847, 866 ],
			
			# Prototype Bomb exposion 
			[ 798, 823, 1012, 1038 ],
			[ 827, 855, 1011, 1023 ],
			[ 860, 888, 1011, 1038 ],
			[ 892, 920, 1010, 1037 ],
			
			# Blue Newtron / Meleon 
			[ 23, 65, 650, 695 ],
			[ 70, 114, 650, 693 ],
			[ 116, 161, 649, 693 ],
			[ 21, 66, 697, 741 ],
			[ 69, 113, 698, 725 ],
			[ 119, 162, 697, 717 ],
			
			# Green Newtron / Meleon 
			[ 178, 221, 520, 564 ],
			[ 178, 221, 568, 612 ],
			[ 224, 269, 521, 565 ],
			[ 225, 270, 569, 598 ],
			[ 273, 317, 521, 564 ],
			[ 274, 317, 568, 588 ],
			
			#
			[ 178, 221, 649, 693 ],
			[ 225, 269, 649, 694 ],
			[ 273, 317, 649, 693 ],
			[ 178, 222, 696, 743 ],
			[ 227, 270, 696, 726 ],
			[ 273, 318, 697, 717 ],
			[ 322, 340, 620, 670 ],
			[ 321, 340, 675, 692 ],

			#Spikes / Yadorin 
			[ 355, 402, 597, 640 ],
			[ 406, 451, 595, 639 ],
			[ 452, 498, 595, 642 ],
			[ 358, 386, 677, 700 ],
			[ 394, 423, 676, 699 ],
			[ 357, 402, 701, 745 ],
			[ 406, 452, 701, 746 ],
			[ 455, 500, 700, 747 ],
			#
			[ 357, 404, 806, 850 ],
			[ 407, 449, 805, 850 ],
			
			# Batbrian / Basaran 
			[ 514, 533, 390, 419 ],
			[ 514, 550, 423, 460 ],
			[ 553, 589, 427, 455 ],
			[ 593, 622, 422, 460 ],
			[ 512, 550, 463, 499 ],
			[ 553, 597, 468, 496 ],
			[ 603, 635, 464, 499 ],
			[ 513, 551, 502, 540 ],
			[ 553, 598, 508, 535 ],
			[ 602, 636, 504, 540 ],
			
			# Roller / Arma 
			[ 513, 550, 601, 652 ],
			#[ 555, 590, 607, 561 ],
			[ 512, 549, 655, 691 ],
			[ 554, 590, 656, 691 ],
			[ 593, 628, 656, 692 ],
			
			# Burrobot / Mogurin 
			[ 652, 681, 442, 489 ],
			[ 685, 714, 443, 491 ],
			[ 717, 746, 444, 492 ],
			[ 751, 780, 447, 492 ],
			[ 654, 682, 495, 546 ],
			[ 685, 713, 497, 545 ],
			[ 718, 748, 497, 542 ],
			[ 751, 779, 495, 542 ],
			[ 653, 685, 551, 589 ],
			[ 690, 721, 550, 591 ],
			[ 732, 773, 561, 590 ],
			[ 654, 685, 593, 634 ],
			[ 690, 720, 594, 632 ],
			
			# Prototype VRAM
			[ 653, 689, 893, 922 ],
			[ 694, 729, 893, 921 ],
			[ 654, 705, 926, 953 ],
			[ 709, 761, 925, 952 ],
			[ 654, 705, 955, 984 ],
			[ 710, 763, 957, 984 ],
			[ 654, 706, 989, 1017 ],
			[ 708, 762, 989, 1017 ],
			[ 653, 706, 1021, 1048 ],
			[ 710, 761, 1021, 1049 ],
		]

	# --------------------------------- Background ----------------------------

	# Contains the coordinates of each parallax strip that is used to construct the full background.
	# Some zones use a single strip to construct their full background.
	# For backgrounds which are constructed out of many parallax strips, the strips can be cropped
	# at different column offsets to get more background combinations.
	# Some zones have a wet and dry version of the background for areas in the level which are under water or dry.
	class BackgroundLookup:
		green_hill_zone = [
			[ 181, 213, 181, 3864 ],
			[ 221, 237, 181, 3864 ],
			[ 245, 261, 181, 3864 ],
			[ 269, 317, 181, 8261 ],	# Can crop right column to 3864 like the others
			[ 325, 365, 181, 8261 ],	# Can crop right column to 3864 like the others
			[ 373, 477, 181, 3864 ],
		]
		
		marble_zone_indoors_act1 = [
			[ 264, 1032, 24, 5144 ],
		]

		marble_zone_indoors_act2 = [
			[ 264, 1032, 24, 6168 ],
		]
				
		marble_zone_indoors_act3 = [
			[ 264, 1288, 24, 5912 ],
		]

		marble_zone_outdoors = [
			[ 264, 280, 24, 1048 ],
			[ 288, 304, 24, 1048 ],
			[ 312, 328, 24, 1048 ],
			[ 336, 352, 24, 1048 ],
			[ 360, 376, 24, 1048 ],
			[ 384, 416, 24, 1048 ],
			[ 424, 568, 24, 1048 ],
		]
	
		labyrinth_zone = [
			[ 320, 1232, 24, 536 ],
		]
			
		spring_yard_zone = [
			[ 264, 280, 24, 2584 ],
			[ 288, 304, 24, 2584 ],
			[ 312, 328, 24, 2584 ],
			[ 336, 352, 24, 2584 ],
			[ 360, 376, 24, 2584 ],
			[ 384, 400, 24, 2584 ],
			[ 408, 440, 24, 2584 ],
			[ 448, 528, 24, 2584 ],
			[ 536, 632, 24, 2584 ],
			[ 640, 656, 24, 536 ],
			[ 664, 680, 24, 536 ],
			[ 668, 704, 24, 536 ],
			[ 712, 728, 24, 536 ],
			[ 736, 752, 24, 536 ],
			[ 760, 776, 24, 536 ],
			[ 784, 792, 24, 536 ],
			[ 800, 808, 24, 536 ],
			[ 816, 824, 24, 536 ],
			[ 832, 840, 24, 536 ],
			[ 848, 856, 24, 536 ],
			[ 864, 872, 24, 536 ],
			[ 880, 888, 24, 536 ],
			[ 896, 904, 24, 536 ],
			[ 912, 920, 24, 536 ],
			[ 928, 936, 24, 536 ],
			[ 944, 952, 24, 536 ],
			[ 960, 968, 24, 536 ],
			[ 976, 984, 24, 536 ],
			[ 992, 1000, 24, 536 ],
		]
		
		star_light_zone = [
			[ 261, 645, 24, 536 ],
			[ 653, 741, 24, 536 ],
			[ 749, 821, 24, 536 ],
			[ 829, 1309, 24, 536 ],
		]
		
		scrap_brain_act1 = [
			[ 264, 280, 24, 1304 ],
			[ 288, 304, 24, 1304 ],
			[ 312, 328, 24, 1304 ],
			[ 336, 352, 24, 1304 ],
			[ 360, 520, 24, 1304 ],
			[ 528, 640, 24, 1304 ],
			[ 648, 824, 24, 1304 ],
		]
			
		scrap_brain_act2 = [
			[ 192, 706, 24, 729 ],
		]
			
		scrap_brain_act3_dry = [
			[ 333, 1245, 24, 536 ],
		]
			
		scrap_brain_act3_wet = [
			[ 333, 1245, 610, 1122 ],
		]
		
	# --------------------------------- Stage Objects -------------------------

	class StageObjectsLookup:
		def __init__(self, image_dir:str):
			spike_log = [
				
			]

			common_objects = [
			
			]

			green_hill_objects = [
				
			]

			scrap_brain_objects = [
				
			]

			labyrinth_objects = [
				
			]

			spring_yard_objects = [
				
			]

			marble_objects = [
				
			]

			star_light_objects = [
				
			]

	# --------------------------------- Stages --------------------------------

	class StagesLookup: # not now
		green_hill_act1 = [
			
		]
			
		green_hill_act2 = [
			
		]

		green_hill_act3 = [
			
		]

		marble_act1 = [
			
		]

		marble_act2 = [
			
		]
		
		marble_act3 = [
		
		]
	
		labyrinth_act1_dry = [
			
		]
			
		labyrinth_act1_wet = [
			
		]
			
		labyrinth_act2_dry = [
			
		]
			
		labyrinth_act2_wet = [
			
		]
			
		labyrinth_act3_dry = [
			
		]
			
		labyrinth_act3_wet = [
			
		]
			
		spring_yard_act1 = [
				
		]
			
		spring_yard_act2 = [
			
		]
			
		spring_yard_act3 = [
			
		]
			
		star_light_act1 = [
			
		]
			
		star_light_act2 = [
			
		]
			
		star_light_act3 = [
			
		]
			
		scrap_brain_act1 = [
			
		]
			
		scrap_brain_act2 = [
			
		]
			
		scrap_brain_act3_dry = [
			
		]
			
		scrap_brain_act3_wet = [
			
		]
				
	# --------------------------------- TileSets ------------------------------

	class TilesetsLookup:
		__all_zones = [		
			[ 264, 520, 24, 280 ],
			[ 264, 520, 288, 544 ],
			[ 264, 520, 552, 808 ],
			[ 264, 520, 816, 1072 ],
			[ 264, 520, 1080, 1336 ],

			[ 528, 784, 24, 280 ],
			[ 528, 784, 288, 544 ],
			[ 528, 784, 552, 808 ],
			[ 528, 784, 816, 1072 ],
			[ 528, 784, 1080, 1336 ],

			[ 792, 1048, 24, 280 ],
			[ 792, 1048, 288, 544 ],
			[ 792, 1048, 552, 808 ],
			[ 792, 1048, 816, 1072 ],
			[ 792, 1048, 1080, 1336 ],

			[ 1056, 1312, 24, 280 ],
			[ 1056, 1312, 288, 544 ],
			[ 1056, 1312, 552, 808 ],
			[ 1056, 1312, 816, 1072 ],
			[ 1056, 1312, 1080, 1336 ],

			[ 1320,	1576, 24, 280 ],
			[ 1320,	1576, 288, 544 ],
			[ 1320,	1576, 552, 808 ],
			[ 1320,	1576, 816, 1072 ],
			[ 1320,	1576, 1080, 1336 ],
			
			[ 1584,	1840, 24, 280 ],
			[ 1584,	1840, 288, 544 ],
			[ 1584,	1840, 552, 808 ],
			[ 1584,	1840, 816, 1072 ],
			[ 1584,	1840, 1080, 1336 ],
			
			[ 1848,	2104, 24, 280 ],
			[ 1848,	2104, 288, 544 ],
			[ 1848,	2104, 552, 808 ],
			[ 1848,	2104, 816, 1072 ],
			[ 1848,	2104, 1080, 1336 ],
			
			[ 2112,	2368, 24, 280 ],
			[ 2112,	2368, 288, 544 ],
			[ 2112,	2368, 552, 808 ],
			[ 2112,	2368, 816, 1072 ],
			[ 2112,	2368, 1080, 1336 ],
			
			[ 2376,	2632, 24, 280 ],
			[ 2376,	2632, 288, 544 ],
			[ 2376,	2632, 552, 808 ],
			[ 2376,	2632, 816, 1072 ],
			[ 2376,	2632, 1080, 1336 ],
			
			[ 2640,	2896, 24, 280 ],
			[ 2640,	2896, 288, 544 ],
			[ 2640,	2896, 552, 808 ],
			[ 2640,	2896, 816, 1072 ],
			[ 2640,	2896, 1080, 1336 ],

			[ 2904,	3160, 24, 280 ],
			[ 2904,	3160, 288, 544 ],
			[ 2904,	3160, 552, 808 ],
			[ 2904,	3160, 816, 1072 ],
			[ 2904,	3160, 1080, 1336 ],

			[ 3168,	3424, 24, 280 ],
			[ 3168,	3424, 288, 544 ],
			[ 3168,	3424, 552, 808 ],
			[ 3168,	3424, 816, 1072 ],
			[ 3168,	3424, 1080, 1336 ],

			[ 3432,	3688, 24, 280 ],
			[ 3432,	3688, 288, 544 ],
			[ 3432,	3688, 552, 808 ],
			[ 3432,	3688, 816, 1072 ],
			[ 3432,	3688, 1080, 1336 ],
		
			[ 3696,	3952, 24, 280 ],
			[ 3696,	3952, 288, 544 ],
			[ 3696,	3952, 552, 808 ],
			[ 3696,	3952, 816, 1072 ],
			[ 3696,	3952, 1080, 1336 ],
		
			[ 3960,	4216, 24, 280 ],
			[ 3960,	4216, 288, 544 ],
			[ 3960,	4216, 552, 808 ],
			[ 3960,	4216, 816, 1072 ],
			[ 3960,	4216, 1080, 1336 ],
		
			[ 4224,	4480, 24, 280 ],
			[ 4224,	4480, 288, 544 ],
			[ 4224,	4480, 552, 808 ],
			[ 4224,	4480, 816, 1072 ],
			[ 4224,	4480, 1080, 1336 ],
		]

		green_hill_zone = __all_zones[ 0 : 11 * 5 - 1 ]
		
		marble_zone = green_hill_zone
		
		# applies to both wet and dry 
		labyrinth_zone = __all_zones[ 0 : 14 * 5 - 3]
		
		spring_yard_zone = __all_zones[ 0 : 14 * 5 ]
		
		star_light_zone = __all_zones[ 0 : 13 * 5 - 3]
			
		scrap_brain_zone_act12 = green_hill_zone
		
		scrap_brain_zone_act3_dry = __all_zones[ 0 : 8 * 5 - 2]
		
		scrap_brain_zone_act3_wet = scrap_brain_zone_act3_dry
		




