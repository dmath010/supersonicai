import numpy as np
import random
import cv2 as cv
import sys
import os

script_dir = os.path.dirname(os.path.abspath(os.path.abspath(__file__)))	# absolute directory of current script's directory
project_dir = os.path.abspath(script_dir + "/../..")						# absolute directory of supersonicai 

sys.path.append(os.path.abspath(project_dir + '/source'))	
sys.path.append(os.path.abspath(project_dir + '/source/vision'))

from globals import * 
from image_file_lookup import ImageFileLookup
from image_crop_lookup import ImageCropLookup
from image_tuple import ImageTuple
from image_processing import *
from color import * 
from file_pair import * 

class Sprites:
	class SpritesBase:
		def __init__(self):
			self.images = []
			#self.results = []
			
		# Label pixels of a segmented image with integer labels.
		# Pixels label is determined by the shade of color of the cooresponding
		#	pixel in the manualy segmented image.
		# `segmented`	manully segmented image (RGB)
		# `bg1_color`	-	Color of all background 1 pixels.	(Stuff we can only look at)
		# `bg2_color`	-	Color of all background 2 pixels.	(Stuff we can walk through)
		# `sg_color`	-	Color of all stage pixels.			(Stuff we can walk on)
		# `mc_color`	-	Color of all mechanical pixels.		(Stuff we can interact with)
		# `shades`		-	Array of tuples
		#						1st - shade of color from manually segmented image
		#						2nd - segmentation label cooresponding to that color.
		def __label_segmented__(self, 
			segmented:np.ndarray,
			shades) -> np.ndarray:
			
			rows = segmented.shape[0]
			cols = segmented.shape[1]

			labels = np.zeros((rows, cols), dtype=np.uint8)

			for shade in shades:
				color = shade[0]
				segmentation_label = shade[1]

				# Determine which pixels are from which segment.
				# Store as binary images (false = 0, true = 255)
				mask = mask_by_color(segmented, color)

				# Set these pixels with their cooresponding integer labels
				labeled_pixels = cv.bitwise_and(mask, segmentation_label)
				
				# Combine labels into one image
				labels = overlay_images(labels, labeled_pixels, mask)

			return labels

		# `image_filepath`	- absolute path to the original and segmented image to be loaded.
		# `crops`			- coordinates of each section (subimage) of both the original and segmented images
		# `canvas_color`	- color of the background pixels of the original (unsegmented) image
		#						This identifies which pixels are to be transparent in both 
		#						the original and segmented images.
		# `shades`		-	Array of tuples
		#						1st - shade of color from manually segmented image
		#						2nd - segmentation label cooresponding to that color.
		def __load_helper__(
			self, 
			image_filepair:FilePair, 
			crops, 
			canvas_color:Color,
			shades):
			original = cv.imread(image_filepair.original)
			segmented = cv.imread(image_filepair.segmentation)
			
			if original is None:
				print("Error:", image_filepair.original, "could not be opened")
				return

			if segmented is None:
				print("Error:", image_filepair.segmentation, "could not be opened")
				return 

			canvas_mask = mask_by_color(original, canvas_color)
			bg_mask = canvas_mask
			fg_mask = cv.bitwise_not(canvas_mask)

			original = cv.bitwise_and(original, original, mask=fg_mask)

			segmented = self.__label_segmented__(segmented, shades)
			
			for crop in crops:
				sprite_original = original[crop[0]:crop[1], crop[2]:crop[3]]
				sprite_segmented = segmented[crop[0]:crop[1], crop[2]:crop[3]]
				sprite_bg_mask = bg_mask[crop[0]:crop[1], crop[2]:crop[3]]
				sprite_fg_mask = fg_mask[crop[0]:crop[1], crop[2]:crop[3]]
				
				image_tuple = ImageTuple(
					original=sprite_original, 
					segmented=sprite_segmented, 
					background_mask=sprite_bg_mask,
					foreground_mask=sprite_fg_mask)
				
				#cv.imshow("tuple", image_tuple.combine())
				#cv.waitKey(0)
				#cv.destroyWindow("tuple")

				self.images.append(image_tuple)
		
		def load(self) -> None:
			return None
		
		# Mask, inverted mask, original, segmented
		def get_all_mask(self):
			for image in self.images:
				original = image
				ring_red = original[:, :, 2]
				ring_green = original[:, :, 1]
				ring_blue = original[:, :, 0]
				_, mask1 = cv.threshold(ring_red, 15, 255, cv.THRESH_BINARY)
				_, mask2 = cv.threshold(ring_green, 75, 255, cv.THRESH_BINARY)
				_, mask3 = cv.threshold(ring_blue, 9, 255, cv.THRESH_BINARY)
				_, mask4 = cv.threshold(ring_red, 11, 255, cv.THRESH_BINARY)
				_, mask5 = cv.threshold(ring_green, 70, 255, cv.THRESH_BINARY)
				_, mask6 = cv.threshold(ring_blue, 5, 255, cv.THRESH_BINARY)
				mask_red = cv.bitwise_xor(mask1, mask4)
				mask_green = cv.bitwise_xor(mask2, mask5)
				mask_blue = cv.bitwise_xor(mask3, mask6)
				mask = cv.bitwise_or(mask_red, mask_green)
				mask = cv.bitwise_or(mask, mask_blue)
				mask = cv.bitwise_not(mask)
				cutout = cv.bitwise_and(original, original, mask=mask)
				inv_mask = cv.bitwise_not(mask)
				print(original)
				#cv.imshow("original", original)
				#cv.imshow("mask", mask)
				#cv.imshow("cutout", cutout)
				#cv.waitKey(0)
				#cv.destroyAllWindows()
				segmented = None
				self.results.append(ImageTuple(original, segmented, inv_mask, mask))
				#self.images.append((original, mask, cutout))

		def stitch(self) -> ImageTuple:
			unsegmented = np.zeros(globals.IMAGE_SIZE_3D, dtype=np.uint8)
			segmented = np.zeros(globals.IMAGE_SIZE_2D, dtype=np.uint8)
			fg_mask = np.zeros(globals.IMAGE_SIZE_2D, dtype=np.uint8)
			bg_mask = np.zeros(globals.IMAGE_SIZE_2D, dtype=np.uint8) + 255

			for i in range(3):
				selection = random.choice(self.images)
				
				nrows = selection.original.shape[0]
				ncols = selection.original.shape[1]
				
				row = random.randint(0, unsegmented.shape[0] - nrows - 1)
				col = random.randint(0, unsegmented.shape[1] - ncols - 1)
				
				unsegmented[row : row + nrows, col : col + ncols, :] = selection.original
				segmented[row : row + nrows, col : col + ncols] = selection.segmented
				fg_mask[row : row + nrows, col : col + ncols] = selection.foreground_mask
				bg_mask[row : row + nrows, col : col + ncols] = selection.background_mask

			return ImageTuple(unsegmented, segmented, fg_mask, bg_mask)
    
	class Sonic(SpritesBase):
		def load(self) -> None:
			shades = [
				( Color(75, 75, 232),	SegmentationLabels.SONIC ),
			]
			
			self.__load_helper__(
				ImageFileLookup.SpritesFileLookup.sonic,
				ImageCropLookup.SpritesLookup.sonic,
				Color(67, 153, 49),
				shades,
			)

	class Eggman(SpritesBase):
		def load(self):
			shades = [
				( Color(75, 75, 232),	SegmentationLabels.ROBOTS ),
			]
			
			self.__load_helper__(
				ImageFileLookup.SpritesFileLookup.eggman,
				ImageCropLookup.SpritesLookup.eggman,
				Color(67, 153, 49),
				shades,
			)
			
	class Badniks(SpritesBase):
		def load(self):
			shades = [
				( Color(246, 12, 21),	SegmentationLabels.ROBOTS ),
			]
			
			self.__load_helper__(
				ImageFileLookup.SpritesFileLookup.badniks,
				ImageCropLookup.SpritesLookup.badniks,
				Color(13, 72, 7),
				shades,
			)
