
import cv2 as cv
import numpy as np
import sys
import os

# Coordinates of each badnick sprite taken from the Badniks image file
badnicks_coords = [
    # crabmeat / ganignani
    [200, 238, 25, 72],    # crab1
    [200, 238, 79, 130],    # crab2
    [197, 240, 136, 185],    # crab3
    [197, 240, 192, 240],    # crab4
    [200, 238, 246, 297],    # crab5
    [200, 214, 305, 322],    # crab6
    [216, 240, 305, 322],    # crab7    # Buzz  bomber / beeron
    [278, 303, 23, 72],     # crab1
    [274, 310, 79, 120],    # crab2
    [274, 292, 135, 153],   # crab3
    [298, 316, 135, 153],   # crab4
    [330, 340, 28, 67],     # crab5
    [346, 379, 23, 72],     # crab6
    [386, 411, 23, 72],     # crab7
    [346, 379, 79, 128],    # crab8    #[386, 379, 411, 128],   # crab9
    [421, 472, 23, 80],     # crab10
    [418, 476, 87, 144],    # crab11
    [414, 423, 27, 67],     # crab12
    [414, 424, 79, 90],     # crab13
    [313, 324, 95, 113],    # crab14
    [323, 339, 135, 153],   # crab15    #Motobug / Motora    [274,308,173 ,222],
    [274,307, 228, 278],
    [274,283, 284, 293],
    [313, 348, 172, 223],
    [313, 347,228, 278],
    [ 290 , 300,284, 294],
    [305 , 317,283 , 295],    # Masher / Batabata
    [381, 415,172, 205],
    #[212,381,414,245],    #Blue Newtron , Meleon ( all) ,
    [521, 564, 22, 66],
    [521, 564, 70, 115],
    [521, 565, 118, 163],
    [569, 612, 22, 66],
    [569, 597, 70, 115],   
    [569, 589, 118, 162],
    [593, 605, 122, 134],
    [593, 604, 138, 157],    #Bomb],
    [791, 827, 22, 49],
    [791, 827, 55, 81],
    [792, 827, 87, 114],
    [791, 827, 118, 146],
    [791, 828, 151, 178],
    [831, 867, 22, 51],
    [832, 867, 56, 81],
    [832, 867, 87, 113],
    [833, 867, 120, 145],
    [831, 867, 152, 178],
    [872, 891, 23, 33],
    [871, 891, 38, 51],
    [879, 971, 70, 81],
    [879, 891, 86, 99],
    [879, 892, 103, 130],
    [879, 892, 134, 163],
    [790, 837, 181, 211],
    [791, 837, 214, 242],
    [791, 837, 247, 275],
    [791, 837, 278, 306],
    [791, 838, 310, 338],
    [841, 888, 182, 210],
    [840, 887, 214, 242],
    [841, 887, 245, 275],
    [840, 888, 278, 306],
    [841, 888, 310, 338],
    [894, 940, 21, 50],
    [895, 939, 54, 83],
    [895, 939, 86, 114],
    [895, 940, 117, 146],
    [895, 940, 150, 178],
    [895, 940, 181, 210],
    [895, 940, 214, 243],
    [895, 940, 246, 275],
    [899, 936, 278, 307],
    [943, 997, 21, 51],
    [942, 996, 54, 82],
    [947, 992, 86, 115],
    [947, 992, 119, 147],
    [945, 992, 150, 181],
    [947, 992, 181, 210],
    [947, 992, 214, 243],
    [947, 992, 246, 274],
    [947, 992, 278, 307],    # caterkiller , Nal],
    [196, 224, 358, 377],
    [196, 224, 381, 402],
    [200, 220, 406, 427],
    [244, 282, 358, 402],
    [245, 282, 406, 451],
    [302, 330, 358, 417],
    [302, 330, 422, 482],    #Spikes,Yadorin ( final ) ],
    [374, 394, 359, 386],
    [375, 396, 395, 425],
    [396, 442, 357, 403],
    [398, 442, 406, 451],
    [398, 442, 453, 499],
    [444, 489, 357, 403],
    [446, 490, 406, 450],
    [445, 489, 454, 499],
    [493, 537, 357, 403],
    [492, 538, 406, 452],
    [492, 536, 454, 499],    #Batbrain, Basaran ( Final, Proper) ],
    [206, 235, 514, 535],
    [206, 218, 537, 550],
    [223, 235, 538, 551],
    [238, 275, 513, 551],
    [243, 272, 553, 591],
    [238, 276, 594, 623],
    [278, 315, 513, 550],
    [282, 312, 554, 599],
    [278, 315, 602, 639],
    [318, 355, 514, 552],
    [322, 351, 553, 598],
    [319, 356, 602, 639],    #Roller , Arma ( Prototype)],
    [788, 843, 513, 550],
    [798, 842, 554, 590],
    [805, 843, 593, 633],
    [845, 883, 514, 551],
    [844, 883, 554, 591],
    [845, 882, 594, 631],
    [901, 939, 513, 550],
    [901, 938, 554, 591],
    [901, 939, 594, 631],    # Burrobot / Mogurin (final)
    [213, 655,264,685],
    [212,268,687,716],
    [217,264,717,748],
    [213, 263,750,781],    
    [265,319,653, 684],
    [266,320,684, 716],
    [265,315,716,749],
    [267,315,751,783],
    [319,362,653,688],
    [319,363,688,723],
    [332,363,731,777],
    [362,406,653,688],
    [318,364,687,726],
    [330,364,731,778],
    [364,409,652,689],
    [364,407,689,724],    #javs/puku-puku (final) 
]

# Absolute path to directory containing all images (originals and segmentations)
images_dir = os.path.join(project_dir, "data/SpritersResource")

# Absolute path to badnicks image file
badniks_original_file = images_dir + r"\Enemies & Bosses\Genesis 32X SCD - Sonic the Hedgehog - Badniks.png"
badniks_segmentation_file = images_dir + r"\Enemies & Bosses\Genesis 32X SCD - Sonic the Hedgehog - Badniks - segmented.png"

# Absolute path to background of Green Hill Zone (Acts 1, 2 and 3)
# *** There is no explicit segmentation for this because it is all one segment ***
green_hill_bg = images_dir + r"\Background\Genesis 32X SCD - Sonic the Hedgehog - Green Hill Zone.png"

badniks_original = cv.imread(badniks_original_file)
badniks_segmentation = cv.imread(badniks_segmentation_file)

#cv.imshow("Badnicks Org", badniks_original)
#cv.imshow("Badnicks Seg", badniks_segmentation)
#cv.waitKey(0)

#for c in badnicks_coords:
#    print(c)
#    original_sub = badniks_original[c[0]:c[1], c[2]:c[3]]
#    segmented_sub = badniks_segmentation[c[0]:c[1], c[2]:c[3]]
#    
#    cv.imshow("original_sub", original_sub)
#    cv.imshow("segmented_sub", segmented_sub)
#    cv.waitKey(1000)
#    #cv.destroyWindow("my subimage")
#    cv.destroyAllWindows()

