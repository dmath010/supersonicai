
##---------------Source-------------------------##
# Montalvo, J., García-Martín, Á. & Bescós, J. Exploiting semantic segmentation to boost reinforcement learning in video game environments. 
# Multimed Tools Appl (2022). https://doi-org.ezproxy.lib.vt.edu/10.1007/s11042-022-13695-1import 
##---------------Source-------------------------##
import os
os.environ['CUDA_LAUNCH_BLOCKING'] = '1'

script_dir = os.path.dirname(os.path.abspath(__file__))
project_dir = os.path.abspath(script_dir + "/../..")



from PIL import Image
import numpy as np
import random
from os.path import join

import torch.utils.data as data
from torchvision import  transforms
import torch.nn.functional as F
import torchvision.transforms.functional as TF

class SonicDataset(data.Dataset):
    def __init__(self, args, mode, transform_input=transforms.ToTensor(), transform_mask=transforms.ToTensor()):
        self.args = args
        self.folder = args.dataset

        #If you change how you create the dataset you may need to modify this:
        self.images_in_dataset = len(os.listdir(self.folder+"/originals"))
        training_images_no = int(self.images_in_dataset*0.8)
        self.imgs = np.arange(training_images_no) if mode == 'train' else np.arange(training_images_no,self.images_in_dataset)

        if len(self.imgs) == 0:
            raise RuntimeError('Found 0 images, please check the data set or proportions')

        self.mode = mode
        self.transform_input = transform_input
        self.transform_mask = transform_mask

    # Default trasnformations on train data
    def transform(self, image, mask):

        i, j, h, w = transforms.RandomCrop.get_params(image, (224,224))
        
        image = TF.crop(image,i,j,h,w)
        mask  = TF.crop(mask,i,j,h,w)

        if random.random() > 0.5:
            image = TF.hflip(image)
            mask  = TF.hflip(mask)

        return image, mask

    # Default trasnformations on test data
    def test_transform(self, image, mask):
        #224x224 center crop: 
        image = TF.center_crop(image,[224,224])
        mask  = TF.center_crop(mask,[224,224])

        return image, mask
    
    def __getitem__(self, index):

        if self.mode == 'test':
            img = Image.open(+self.folder+"/originals/"+str(index)+".png").convert('RGB')
            if self.transform is not None:
                img = self.transform(img)
            return str(index), img

        #Load RGB image
        img = Image.open(self.folder+"/originals/"+str(index)+".png").convert('RGB')

        if self.mode == 'train':

            #Load class mask
            mask = Image.open(self.folder+"/segmentations/"+str(index)+".png")
        else:
            mask = Image.open(self.folder+"/segmentations/"+str(index)+".png")

            ##Transform using default transformations
        if self.mode=="train":
              img, mask = self.transform(img,mask)
        else:
              img, mask = self.test_transform(img,mask)

        if self.transform_input is not None:
           img = self.transform_input(img)
        if self.transform_mask is not None:
            mask = 255*self.transform_mask(mask)

        return img, mask.long()

    def __len__(self):
        return len(self.imgs)
