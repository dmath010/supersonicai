import os
import sys

script_dir = os.path.dirname(os.path.abspath(os.path.abspath(__file__)))	# absolute directory of current script's directory
project_dir = os.path.abspath(script_dir + "/../..")						# absolute directory of supersonicai 

sys.path.append(os.path.abspath(project_dir + '/source/dataset/'))	

from image_file_lookup import *
from image_crop_lookup import * 
from file_pair import *

# Purpose: Saves the file paths of image files of both original and manually segmented images.
#	These images will be used to construct an image segmentation training dataset.
#	Images files are stored in pairs (one for the original and one for the manual segmentation)
#	For images containing sprites, this class stores the coordinates of each sprite
#	(both for original and manual segmentations).
#
#	Image Pairs:
#		Original Images - Images stored in png files which contain the original pixelated image of maps and sprites. 
#		Segmented Images - Images stored in png files which contain the manually segmented images of maps and sprites. 
#		*** In all images, pixels which are not part of the "thing" should be marked as transparent.
image_dir = os.path.join(project_dir, "data/SpritersResource")
class ImageFileLookup:
	class SpritesFileLookup():
		sonic_dir = os.path.join(image_dir, "Playable Characters")
		enemies_dir = os.path.join(image_dir, "Enemies & Bosses")
		
		# --- Sonic ---
		sonic = FilePair(\
			sonic_dir, \
			"Genesis 32X SCD - Sonic the Hedgehog - Sonic the Hedgehog.png", \
			"Genesis 32X SCD - Sonic the Hedgehog - Sonic the Hedgehog - segmented.png")
		
		# --- Eggman ---
		eggman = FilePair(\
			enemies_dir, \
			"Genesis 32X SCD - Sonic the Hedgehog - Dr Robotnik Eggman.png", \
			"Genesis 32X SCD - Sonic the Hedgehog - Dr Robotnik Eggman - segmented.png")
			
		# --- Badniks ---
		badniks = FilePair(\
			enemies_dir, \
			"Genesis 32X SCD - Sonic the Hedgehog - Badniks.png", \
			"Genesis 32X SCD - Sonic the Hedgehog - Badniks - segmented.png")

	class BackgroundLookup:
			
		# *** These images don't have explicit segmentations because the all pixels ***
		# *** in the backgrounds are part of the same segment. ***
	
		background_dir = os.path.join(image_dir, "Background")
			
		green_hill_zone = os.path.join(background_dir, "Genesis 32X SCD - Sonic the Hedgehog - Green Hill Zone.png")
		labyrinth_zone = os.path.join(background_dir, "Genesis 32X SCD - Sonic the Hedgehog - Labyrinth Zone.png")
		marble_zone_indoors_act1 = os.path.join(background_dir, "Genesis 32X SCD - Sonic the Hedgehog - Marble Zone Indoors Act 1.png")
		marble_zone_indoors_act2 = os.path.join(background_dir, "Genesis 32X SCD - Sonic the Hedgehog - Marble Zone Indoors Act 2.png")
		marble_zone_indoors_act3 = os.path.join(background_dir, "Genesis 32X SCD - Sonic the Hedgehog - Marble Zone Indoors Act 3.png")
		marble_zone_outdoors = os.path.join(background_dir, "Genesis 32X SCD - Sonic the Hedgehog - Marble Zone Outdoors USA.png")
		spring_yard_zone = os.path.join(background_dir, "Genesis 32X SCD - Sonic the Hedgehog - Spring Yard Zone USA.png")
		star_light_zone = os.path.join(background_dir, "Genesis 32X SCD - Sonic the Hedgehog - Star Light Zone.png")
		scrap_brain_zone_act1 = os.path.join(background_dir, "Genesis 32X SCD - Sonic the Hedgehog - Scrap Brain Zone Act 1 USA.png")
		scrap_brain_zone_act2 = os.path.join(background_dir, "Genesis 32X SCD - Sonic the Hedgehog - Scrap Brain Zone Act 2 & Final Zone.png")
		scrap_brain_zone_act3 = os.path.join(background_dir, "Genesis 32X SCD - Sonic the Hedgehog - Scrap Brain Zone Act 3.png")
			
	class StageObjectsLookup:
		stage_objects_dir = os.path.join(image_dir, "Stage Objects")

		# TODO: still need manual segmentations of these

		spike_log = FilePair(\
			stage_objects_dir,\
			"SpikeLog1.png",\
			"SpikeLog1 - segmented.png")	# TODO: segment

		common_objects = FilePair(\
			stage_objects_dir,\
			"Genesis 32X SCD - Sonic the Hedgehog - Objects Common.png",\
			"Genesis 32X SCD - Sonic the Hedgehog - Objects Common - segmented.png")

		green_hill_objects = FilePair(\
			stage_objects_dir,\
			"Genesis 32X SCD - Sonic the Hedgehog - Green Hill Zone Objects.png",\
			"Genesis 32X SCD - Sonic the Hedgehog - Green Hill Zone Objects - segmented.png")

		scrap_brain_objects = FilePair(\
			stage_objects_dir,\
			"Genesis 32X SCD - Sonic the Hedgehog - Scrap Brain Zone Objects.png",\
			"Genesis 32X SCD - Sonic the Hedgehog - Scrap Brain Zone Objects - segmented.png")

		labyrinth_objects = FilePair(\
			stage_objects_dir,\
			"Genesis 32X SCD - Sonic the Hedgehog - Labyrinth Zone & Scrap Brain Zone Act 3 Objects.png",\
			"Genesis 32X SCD - Sonic the Hedgehog - Labyrinth Zone & Scrap Brain Zone Act 3 Objects - segmented.png")

		spring_yard_objects = FilePair(\
			stage_objects_dir,\
			"Genesis 32X SCD - Sonic the Hedgehog - Spring Yard Zone Objects.png",\
			"Genesis 32X SCD - Sonic the Hedgehog - Spring Yard Zone Objects - segmented.png")

		marble_objects = FilePair(\
			stage_objects_dir,\
			"Genesis 32X SCD - Sonic the Hedgehog - Marble Zone Objects.png",\
			"Genesis 32X SCD - Sonic the Hedgehog - Marble Zone Objects - segmented.png")

		star_light_objects = FilePair(\
			stage_objects_dir,\
			"Genesis 32X SCD - Sonic the Hedgehog - Star Light Zone Objects.png",\
			"Genesis 32X SCD - Sonic the Hedgehog - Star Light Zone Objects - segmented.png")

	class StagesLookup:
		stages_dir = os.path.join(image_dir, "Stages")

		green_hill_act1 = FilePair(
			stages_dir,\
			"Genesis 32X SCD - Sonic the Hedgehog - Green Hill Zone Act 1.png",\
			"Genesis 32X SCD - Sonic the Hedgehog - Green Hill Zone Act 1 - segmented.png",)
			
		green_hill_act2 = FilePair(\
			stages_dir,\
			"Genesis 32X SCD - Sonic the Hedgehog - Green Hill Zone Act 2.png",\
			"Genesis 32X SCD - Sonic the Hedgehog - Green Hill Zone Act 2 - segmented.png")

		green_hill_act3 = FilePair(\
			stages_dir,\
			"Genesis 32X SCD - Sonic the Hedgehog - Green Hill Zone Act 3.png",\
			"Genesis 32X SCD - Sonic the Hedgehog - Green Hill Zone Act 3 - segmented.png")

		marble_act1 = FilePair(\
			stages_dir,\
			"Genesis 32X SCD - Sonic the Hedgehog - Marble Zone Act 1.png",\
			"Genesis 32X SCD - Sonic the Hedgehog - Marble Zone Act 1 - segmented.png")

		marble_act2 = FilePair(\
			stages_dir,\
			"Genesis 32X SCD - Sonic the Hedgehog - Marble Zone Act 2.png",\
			"Genesis 32X SCD - Sonic the Hedgehog - Marble Zone Act 2 - segmented.png")

		marble_act3 = FilePair(\
			stages_dir,\
			"Genesis 32X SCD - Sonic the Hedgehog - Marble Zone Act 3.png",\
			"Genesis 32X SCD - Sonic the Hedgehog - Marble Zone Act 3 - segmented.png")

		labyrinth_act1_dry = FilePair(\
			stages_dir,\
			"Genesis 32X SCD - Sonic the Hedgehog - Labyrinth Zone Act 1 Dry.png",\
			"Genesis 32X SCD - Sonic the Hedgehog - Labyrinth Zone Act 1 Dry - segmented.png")

		labyrinth_act1_wet = FilePair(\
			stages_dir,\
			"Genesis 32X SCD - Sonic the Hedgehog - Labyrinth Zone Act 1 Underwater.png",\
			"Genesis 32X SCD - Sonic the Hedgehog - Labyrinth Zone Act 1 Underwater - segmented.png")

		labyrinth_act2_dry = FilePair(\
			stages_dir,\
			"Genesis 32X SCD - Sonic the Hedgehog - Labyrinth Zone Act 2 Dry.png",\
			"Genesis 32X SCD - Sonic the Hedgehog - Labyrinth Zone Act 2 Dry - segmented.png")

		labyrinth_act2_wet = FilePair(\
			stages_dir,\
			"Genesis 32X SCD - Sonic the Hedgehog - Labyrinth Zone Act 2 Underwater.png",\
			"Genesis 32X SCD - Sonic the Hedgehog - Labyrinth Zone Act 2 Underwater - segmented.png")

		labyrinth_act3_dry = FilePair(\
			stages_dir,\
			"Genesis 32X SCD - Sonic the Hedgehog - Labyrinth Zone Act 3 Dry.png",\
			"Genesis 32X SCD - Sonic the Hedgehog - Labyrinth Zone Act 3 Dry - segmented.png")

		labyrinth_act3_wet = FilePair(\
			stages_dir,\
			"Genesis 32X SCD - Sonic the Hedgehog - Labyrinth Zone Act 3 Underwater.png",\
			"Genesis 32X SCD - Sonic the Hedgehog - Labyrinth Zone Act 3 Underwater - segmented.png")

		spring_yard_act1 = FilePair(\
			stages_dir,\
			"Genesis 32X SCD - Sonic the Hedgehog - Spring Yard Zone Act 1.png",\
			"Genesis 32X SCD - Sonic the Hedgehog - Spring Yard Zone Act 1 - segmented.png")

		spring_yard_act2 = FilePair(\
			stages_dir,\
			"Genesis 32X SCD - Sonic the Hedgehog - Spring Yard Zone Act 2.png",\
			"Genesis 32X SCD - Sonic the Hedgehog - Spring Yard Zone Act 2 - segmented.png")

		spring_yard_act3 = FilePair(\
			stages_dir,\
			"Genesis 32X SCD - Sonic the Hedgehog - Spring Yard Zone Act 3.png",\
			"Genesis 32X SCD - Sonic the Hedgehog - Spring Yard Zone Act 3 - segmented.png")

			
		star_light_act1 = FilePair(\
			stages_dir,\
			"Genesis 32X SCD - Sonic the Hedgehog - Star Light Zone Act 1.png",\
			"Genesis 32X SCD - Sonic the Hedgehog - Star Light Zone Act 1 - segmented.png")

		star_light_act2 = FilePair(\
			stages_dir,\
			"Genesis 32X SCD - Sonic the Hedgehog - Star Light Zone Act 2.png",\
			"Genesis 32X SCD - Sonic the Hedgehog - Star Light Zone Act 2 - segmented.png")

		star_light_act3 = FilePair(\
			stages_dir,\
			"Genesis 32X SCD - Sonic the Hedgehog - Star Light Zone Act 3.png",\
			"Genesis 32X SCD - Sonic the Hedgehog - Star Light Zone Act 3 - segmented.png")
	
		scrap_brain_act1 = FilePair(\
			stages_dir,\
			"Genesis 32X SCD - Sonic the Hedgehog - Scrap Brain Zone Act 1.png",\
			"Genesis 32X SCD - Sonic the Hedgehog - Scrap Brain Zone Act 1 - segmented.png")

		scrap_brain_act2 = FilePair(\
			stages_dir,\
			"Genesis 32X SCD - Sonic the Hedgehog - Scrap Brain Zone Act 2 & Final Zone.png",\
			"Genesis 32X SCD - Sonic the Hedgehog - Scrap Brain Zone Act 2 & Final Zone - segmented.png")

		scrap_brain_act3_dry = FilePair(\
			stages_dir,\
			"Genesis 32X SCD - Sonic the Hedgehog - Scrap Brain Zone Act 3 Dry.png",\
			"Genesis 32X SCD - Sonic the Hedgehog - Scrap Brain Zone Act 3 Dry - segmented.png")

		scrap_brain_act3_wet = FilePair(\
			stages_dir,\
			"Genesis 32X SCD - Sonic the Hedgehog - Scrap Brain Zone Act 3 Underwater.png",\
			"Genesis 32X SCD - Sonic the Hedgehog - Scrap Brain Zone Act 3 Underwater - segmented.png")

	class TilesetsLookup:
		tilesets_dir = os.path.join(image_dir, "Tilesets")

		green_hill_zone = FilePair(\
			tilesets_dir,\
			"Genesis 32X SCD - Sonic the Hedgehog - Green Hill Zone & Ending.png",\
			"Genesis 32X SCD - Sonic the Hedgehog - Green Hill Zone & Ending - segmented.png")
			
		marble_zone = FilePair(\
			tilesets_dir,\
			"Genesis 32X SCD - Sonic the Hedgehog - Marble Zone.png",\
			"Genesis 32X SCD - Sonic the Hedgehog - Marble Zone - segmented.png")
			
		labyrinth_zone_dry = FilePair(\
			tilesets_dir,\
			"Genesis 32X SCD - Sonic the Hedgehog - Labyrinth Zone.png",\
			"Genesis 32X SCD - Sonic the Hedgehog - Labyrinth Zone - segmented.png")
			
		labyrinth_zone_wet = FilePair(\
			tilesets_dir,\
			"Genesis 32X SCD - Sonic the Hedgehog - Labyrinth Zone Underwater.png",\
			"Genesis 32X SCD - Sonic the Hedgehog - Labyrinth Zone - segmented.png")
		
		spring_yard_zone = FilePair(\
			tilesets_dir,\
			"Genesis 32X SCD - Sonic the Hedgehog - Spring Yard Zone.png",\
			"Genesis 32X SCD - Sonic the Hedgehog - Spring Yard Zone - segmented.png")
			
		star_light_zone = FilePair(\
			tilesets_dir,\
			"Genesis 32X SCD - Sonic the Hedgehog - Star Light Zone.png",\
			"Genesis 32X SCD - Sonic the Hedgehog - Star Light Zone - segmented.png")
			
		scrap_brain_zone_act12 = FilePair(\
			tilesets_dir,\
			"Genesis 32X SCD - Sonic the Hedgehog - Scrap Brain Zone Acts 1 & 2 and Final Zone.png",\
			"Genesis 32X SCD - Sonic the Hedgehog - Scrap Brain Zone Acts 1 & 2 and Final Zone - segmented.png")
			
		scrap_brain_zone_act3_dry = FilePair(\
			tilesets_dir,\
			"Genesis 32X SCD - Sonic the Hedgehog - Scrap Brain Zone Act 3 Dry.png",\
			"Genesis 32X SCD - Sonic the Hedgehog - Scrap Brain Zone Act 3 Dry - segmented.png")
			
		scrap_brain_zone_act3_wet = FilePair(\
			tilesets_dir,\
			"Genesis 32X SCD - Sonic the Hedgehog - Scrap Brain Zone Act 3 Underwater.png",\
			"Genesis 32X SCD - Sonic the Hedgehog - Scrap Brain Zone Act 3 Underwater - segmented.png")
			