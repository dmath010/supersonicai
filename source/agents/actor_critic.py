# based off of these pages:
# MNIST CNN:	https://medium.com/@nutanbhogendrasharma/pytorch-convolutional-neural-network-with-mnist-dataset-4e8a4265e118
# MNIST CNN:	https://github.com/stabgan/CNN-classification-of-MNIST-dataset-using-pyTorch/blob/master/cnn.py
# Actor Critic: https://github.com/nikhilbarhate99/Actor-Critic-PyTorch/blob/master/model.py

#from importlib.metadata import distribution, requires
import sys
import os
import torch 
from torch.autograd import Variable
import torch.nn.functional as F
from torch.distributions import Categorical

script_dir = os.path.dirname(os.path.abspath(__file__))
project_dir = os.path.abspath(script_dir + "/../..")

sys.path.append(os.path.abspath(project_dir + '/source/agents'))
sys.path.append(os.path.abspath(project_dir + '/source/interface'))
sys.path.append(os.path.abspath(project_dir + '/source/learning'))

from agent_base import *
from action_space import *
from train_actor_critic import * 

# A decision making agent implemented as a neural network based on the Actor Critic Model.
# Trained using the Actor Critic Method.
class ActorCritic(AgentBase):

	# ----------------------- Nested Classes ----------------------------------

	class Actor(torch.nn.Module):
		def __init__(self):
			self.make_model()
		
		def make_model(self):
			super(ActorCritic.Actor, self).__init__()

			# --- Skip Input Layer ---
			# TODO: define input layer explicitly
			# For now, input layer will be created on the 1st forward pass.

			# *** Shouldn't we use Conv3d for colored images? ***
			# *** No Conv2d is for both gray and colored images ***

			# --- Hidden Layer 1 ---
			
			self.conv1 = torch.nn.Sequential(
				torch.nn.Conv2d(
					in_channels=3,		# 3 channel color image
					out_channels=1, #16,	# I think this means each convolutional kernel will output 16 nodes. See below ***
					kernel_size=1, #5,		# Size of each kernel in both dimentions: 5x5. I think this should be the size of each object in the game.
					stride=1,			# Create a kernel on every so many pixels. 1 means don't skip any pixels.
					padding=2,			# ???
				),
				torch.nn.ReLU(),		# activation function. experiment with this. Try sigmoid.
				torch.nn.MaxPool2d(2),	# Pick the highest outputs of each group of 2x2 nodes.
			)
			
			# --- Hidden Layer 2 ---
			self.conv2 = torch.nn.Sequential(
				torch.nn.Conv2d(
					in_channels=1, #16,		# TODO: This should equal to out_channels from the previous layer. Should not be hard coded.		
					out_channels=1, #32,	# TODO: experiment with this
					kernel_size=1, #5,		# TODO: experiment with this
					stride=1,			# 
					padding=2,			# 
				),
				torch.nn.ReLU(),		# 
				torch.nn.MaxPool2d(2),	# 
				)

			# --- Output Layers ---
			self.actor = torch.nn.Sequential(
				torch.nn.Linear(
					in_features=4897, #143360,
					out_features=ActionSpace.get_n_moves(),	# 1 output for each possible move. See class ActionSpace
				),
				torch.nn.Sigmoid(),							# Maybe use sigmoid to cap range to [0.0, 1.0]
			)
			
			# *** I think out_channels is the number of output nodes that each kernel outputs.
			# For example, out_channels=16 creates 16 output nodes for each kernel. 
			# In any NN, each node extracts a feature from its input.
			# out_channels says how many different features to extract from that kernel.
			# This is like saying 16 different features will be extracted from each kernel.
			# The more features, the more the NN can learn.
			# Should this number correspond to the the number of output classes???
			# Its worth testing.

			return

		def forward(self, x):
			x = x.reshape((3, 224, 320))
			x = torch.unsqueeze(x, 0)
			x = x.float()
			x = self.conv1(x)
			x = self.conv2(x)
			x = x.view(x.size(0), -1)
			x = self.actor(x)		# Get action from actor layer
			distribution = Categorical(F.softmax(x, dim=1))	# TODO: Try dim=-1
			return distribution
		
	class Critic(torch.nn.Module):
		def __init__(self):
			self.make_model()
		
		def make_model(self):
			super(ActorCritic.Critic, self).__init__()

			self.conv1 = torch.nn.Sequential(
				torch.nn.Conv2d(
					in_channels=3,		# 3 channel color image
					out_channels=1, #16,	# I think this means each convolutional kernel will output 16 nodes. See below ***
					kernel_size=1, #5,		# Size of each kernel in both dimentions: 5x5. I think this should be the size of each object in the game.
					stride=1,			# Create a kernel on every so many pixels. 1 means don't skip any pixels.
					padding=2,			# ???
				),
				torch.nn.ReLU(),		# activation function. experiment with this. Try sigmoid.
				torch.nn.MaxPool2d(2),	# Pick the highest outputs of each group of 2x2 nodes.
			)
			
			self.conv2 = torch.nn.Sequential(
				torch.nn.Conv2d(
					in_channels=1, #16,		# TODO: This should equal to out_channels from the previous layer. Should not be hard coded.		
					out_channels=1, #32,	# TODO: experiment with this
					kernel_size=1, #5,		# TODO: experiment with this
					stride=1,			# 
					padding=2,			# 
				),
				torch.nn.ReLU(),		# 
				torch.nn.MaxPool2d(2),	# 
				)

			self.critic = torch.nn.Sequential(
				torch.nn.Linear(
					in_features=4897, #143360,
					out_features=1,	# 1 output for each possible move. See class ActionSpace
				),
				torch.nn.Sigmoid(),							# Maybe use sigmoid to cap range to [0.0, 1.0]
			)

		def forward(self, x):
			x = x.reshape((3, 224, 320))
			x = torch.unsqueeze(x, 0)
			x = x.float()
			x = self.conv1(x)
			x = self.conv2(x)
			x = x.view(x.size(0), -1)
			x = self.critic(x)		# Get action from actor layer

			return x
	
	# --------------------------------- METHODS -------------------------------

	# Creates an untrained neural network.
	# Weights and biases are initialized to random numbers.
	# image_size is a tuple in the form (3, width, height).
	# Inputs RGB images.
	# This initializes input layer of NN to match the input image.
	def __init__(self):
		self.make_model()
		
	# Creates a neural network and initializes it with random weights and biases.
	def make_model(self):
		self.actor = ActorCritic.Actor()
		self.critic = ActorCritic.Critic()

		self.actor.make_model()
		self.critic.make_model()

	# Executes a forward pass on input tensor x.
	# 1st dimension of x should be the batches of samples. 
	# The size of the 1st dimention is the batch size (1 in our case).
	# 2nd, 3rd and 4th dimensions are the dimensions of the image. This is implicitly determined on the 1st forward pass.
	# 
	# 1st - batches (1 sample per batch)
	# 2nd - number of color channels (3 for color, 1 for gray)
	# 3rd - image height in pixels
	# 4th - image width in pixels
	# 
	# *** This method must be implemented so that torch.nn.Module can execute forward passes. ***
	# It must be spelled "forward"
	# It must accept 1 parameter which is a torch.Tensor
	def forward(self, x):
		output = self.actor.forward(x)
		
		return output

	# Loads a NN from a file
	def load(self, filename) -> None:
		##self.actor = torch.load('actor_19000.pt')
		##self.critic = torch.load('critic_19000.pt')
		None

	# Saves NN to a file
	# filename - file to save model in. Extension should be .pth 
	def save(self, filename) -> None:
		torch.save(self.state_dict(), filename)
	
	# Trains NN for one epoch
	def train(self, env, n_epochs, reward, render, ckpt, save_rate) -> None:
		ActorCriticTrainer.train_actor_critic( \
			agent=self, \
			env=env, \
			n_epochs=n_epochs, \
			reward=reward, \
			render=render, \
			checkpoint=ckpt, \
			save_rate=save_rate)

	# Chooses an action based on the current state of the game.
	# obs - numpy.ndarray representing the game frame. 
	# info - information about the game state like time, position, and ring count.
	# returns - array of button presses. See class ActionSpace.
	def decide(self, obs, info) -> list:
		
		# 1.) Compute forward pass
		# TODO: Compute on GPU if available. See if that is faster.
		# output = output.cuda()
		img = torch.from_numpy(obs)

		output = self.forward(img)
		
		output = output.sample()

		# 2.) Convert int to array of button presses.
		buttons = ActionSpace.move(output.cpu().item())
		
		return buttons
		
	# Returns name of agent as a string
	def name(self) -> str:
			return "ActorCritic"
	
	def to_string(self) -> str:
		return self.name()

	# Moves data from current memory to 'device' memory
	# ex: agent.move_to(torch.KGPU) will move neural network data to GPU memory. 
	# If sucessfull, all operations on this NN will be executed on that device (CPU or GPU).
	# Internal fields will be moved. The object itself does not need to be reassigned like tensors do.
	def to(self, device) -> None:
		self.actor = self.actor.to(device)
		self.critic = self.critic.to(device)
		
		