import sys
import os

script_dir = os.path.dirname(os.path.abspath(__file__))
project_dir = os.path.abspath(script_dir + "/../..")

sys.path.append(os.path.abspath(project_dir + '/source/agents'))
sys.path.append(os.path.abspath(project_dir + '/source/interface'))

from actor_critic import *
from actor_critic_a import *
from deep_q_agent import *
from neat_agent import *
from random_agent import *
from replay_agent import *
