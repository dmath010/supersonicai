import sys
import os
import random
from action_space import ActionSpace

script_dir = os.path.dirname(os.path.abspath(__file__))
project_dir = os.path.abspath(script_dir + "/../..")

sys.path.append(os.path.abspath(project_dir + '/source/agents'))
sys.path.append(os.path.abspath(project_dir + '/source/interface'))

from agent_base import *
from action_space import *

class RandomAgent(AgentBase):
	def load(self, filename):
		None	# nothing to do here

	def save(self, filename):
		None	# nothing to do here

	def train(self):
		None	# nothing to do here

	def decide(self, obs, info) -> list:
		# 1.) Generate a random number
		num = random.randint(0, ActionSpace.get_n_moves() - 1)

		# 2.) Pick a random move
		return ActionSpace.move(num)

	# Returns name of agent as a string
	def name(self) -> str:
		return "RandomAgent"

	def to_string(self) -> str:
		return self.name()