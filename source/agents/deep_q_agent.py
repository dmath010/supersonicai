
##---------------Sources-------------------------##
# DeepQ Learning with PyTorch: https://pytorch.org/tutorials/intermediate/reinforcement_q_learning.html
# DeepQ Image Processing for GymRetro:  https://github.com/deepanshut041/Reinforcement-Learning 
##------------------------------------------------##


import sys
import os

import numpy as np
import torch
import torch.nn.functional as F
import torch.optim as optim
import random

script_dir = os.path.dirname(os.path.abspath(__file__))
project_dir = os.path.abspath(script_dir + "/../..")

sys.path.append(os.path.abspath(project_dir + '/source/agents'))
sys.path.append(os.path.abspath(project_dir + '/source/interface'))
sys.path.append(os.path.abspath(project_dir + '/source/learning'))
sys.path.append(os.path.abspath(project_dir + '/source/models'))
sys.path.append(os.path.abspath(project_dir + '/source/datasets'))
sys.path.append(os.path.abspath(project_dir + '/source/vision'))

from deeplab import *
from deeplab_dataset import *
from agent_base import * 
from deep_q_buffer import *
from train_deep_q import *
from deep_q_model import DQN
from datetime import datetime, date
from action_space import *
from deeplab import *
import time

# DeepQ Neural Network.
class DeepQ(AgentBase):

	# TODO Empty Constructor
	def __init__(self, model=None, seg_model=None):
		"""Initialize an Agent object.
		
		Params
		======
			input_shape (tuple): dimension of each state (C, H, W)
			action_size (int): dimension of each action
			seed (int): random seed
			device(string): Use Gpu or CPU
			buffer_size (int): replay buffer size
			batch_size (int):  minibatch size
			gamma (float): discount factor
			lr (float): learning rate 
			update_every (int): how often to update the network
			replay_after (int): After which replay to be started
			model(Model): Pytorch Model
		"""
		input_shape = (4, 84, 84) #stacked frames x channels x w x h
		self.action_size = 7 #len(possible_actions)
		self.seed = 0 #random.seed(seed)
		self.buffer_size = 100000
		self.batch_size = 32
		self.gamma = 0.99
		self.lr = 0.00001
		self.update_every = 100
		self.replay_after = 10000
		self.device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
		
		self.DQN = DQN
		self.tau = 1e-3

		# Seg Model
		if seg_model is not None:
			timer = time.time()
			self.seg_model = DeepLab(os.path.join(project_dir, seg_model))
			self.seg_model.model.eval()
			print (f'SegModel Instantiaion with {seg_model}: {time.time()-timer}')
		# Q-Network
	
		self.policy_net = self.DQN(input_shape, self.action_size, self.seed).to(self.device)
		if model is not None:
			os.chdir(script_dir)
			os.chdir('..')
			os.chdir('..')
			root = os.getcwd()
			checkpoint = torch.load(model, map_location=self.device)

			self.policy_net.load_state_dict(checkpoint['model_state_dict'])

			#self.policy_net.load_state_dict(torch.load(os.path.join(root, model), map_location=self.device), strict=False)
		self.target_net = self.DQN(input_shape, self.action_size, self.seed).to(self.device)
		self.optimizer = optim.Adam(self.policy_net.parameters(), lr=self.lr)
		
		# Replay memory
		self.memory = ReplayBuffer(self.buffer_size, self.batch_size, self.seed, self.device)
		self.t_step = 0

	def step(self, state, action, reward, next_state, done):
		# Save experience in replay memory
		self.memory.add(state, action, reward, next_state, done)
		
		# Learn every UPDATE_EVERY time steps.
		self.t_step = (self.t_step + 1) % self.update_every

		if self.t_step == 0:
			# If enough samples are available in memory, get random subset and learn
			if len(self.memory) > self.replay_after:
				experiences = self.memory.sample()
				self.learn(experiences)
				
	def act(self, state, eps=0.03):
		"""Returns actions for given state as per current policy."""
		
		state = torch.from_numpy(state).unsqueeze(0).to(self.device)
		self.policy_net.eval()
		with torch.no_grad():
			action_values = self.policy_net(state)
		self.policy_net.train()
		
		# Epsilon-greedy action selection
		if random.random() > eps:
			return np.argmax(action_values.cpu().data.numpy())
		else:
			return random.choice(np.arange(self.action_size))
		
	def learn(self, experiences):
		states, actions, rewards, next_states, dones = experiences

		# Get expected Q values from policy model
		Q_expected_current = self.policy_net(states)
		Q_expected = Q_expected_current.gather(1, actions.unsqueeze(1)).squeeze(1)

		# Get max predicted Q values (for next states) from target model
		Q_targets_next = self.target_net(next_states).detach().max(1)[0]
		
		# Compute Q targets for current states 
		Q_targets = rewards + (self.gamma * Q_targets_next * (1 - dones))
		
		# Compute loss
		loss = F.mse_loss(Q_expected, Q_targets)

		# Minimize the loss
		self.optimizer.zero_grad()
		loss.backward()
		self.optimizer.step()
		self.soft_update(self.policy_net, self.target_net, self.tau)

	def soft_update(self, policy_model, target_model, tau):
		for target_param, policy_param in zip(target_model.parameters(), policy_model.parameters()):
			target_param.data.copy_(tau*policy_param.data + (1.0-tau)*target_param.data)
		something = 0

	def load(self, filename):
		None

	def save(self, filename, epoch):
		torch.save({
			'epoch': epoch,
            'model_state_dict': self.policy_net.state_dict(),
            }, filename)
		
	def train(self, env, n_episodes, reward_system, render, ckpt, save_rate):
		DeepQTrainer.train(self, env, n_episodes, reward_system, render, ckpt, save_rate, self.seg_model)
	
	def decide(self, ob, info) -> list:
		# Quick Fix
		if hasattr(self, '__prev_state'):
			self.__prev_state = DeepQTrainer.stack_frames(self.__prev_state, ob, False, self.seg_model)
		else:
			self.__prev_state = DeepQTrainer.stack_frames(None, ob, True, self.seg_model) 
		
		move = self.act(self.__prev_state)

		return ActionSpace.move(move)

	# Returns name of agent as a string
	def name(self) -> str:
		return "DeepQ"

	# Moves data from current memory to 'device' memory
	# ex: agent.move_to(torch.cuda()) will move neural network data to GPU memory. 
	# If sucessfull, all operations on this NN will be executed on that device (CPU or GPU).
	# Internal fields will be moved. The object itself does not need to be reassigned like tensors do.
	def to(self, device) -> None:
		self.policy_net = self.policy_net.to(device)
		self.target_net = self.target_net.to(device)
		