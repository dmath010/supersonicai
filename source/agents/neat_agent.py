import sys
import os

script_dir = os.path.dirname(os.path.abspath(__file__))
project_dir = os.path.abspath(script_dir + "/../..")

sys.path.append(os.path.abspath(project_dir + '/source/agents'))
sys.path.append(os.path.abspath(project_dir + '/source/interface'))
sys.path.append(os.path.abspath(project_dir + '/source/learning'))

from agent_base import *
from action_space import *
from train_neat import *

class NeatAgent(AgentBase):

	def load(self, filename):
		None
		# TODO:

	def save(self, filename):
		None
		# TODO:
		
	def train(self):
		train_neat()

	def decide(self, obs, info) -> list:
		buttons = ActionSpace.move_right()	

		return buttons

	# Returns name of agent as a string
	def name(self) -> str:
		return "NeatAgent"

	def to_string(self) -> str:
		return self.name()
		

