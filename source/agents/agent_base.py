import sys
import os

script_dir = os.path.dirname(os.path.abspath(__file__))
project_dir = os.path.abspath(script_dir + "/../..")

# Base class of all agents. Agents can be neural networks, random agents, even playbacks, etc.
# All classes deriving from AgentBase need to implement (override) the methods defined below.
# Any method which is called but not implemented will print an error message with a 
# reminder to implement the method. 
# Some implementations will be empty.
# The main purpose of class AgentBase is to server as a reminder and framework for a class heirarchy and polymorphism. 
# In Python everything is polymorphism.
# 
# *Some method may not be relevant to the agent but should be implemented anyway to prevent runtime error messages.
class AgentBase:
	# Does not need to be overriden
	def __print_error_message_1(method_name):
		print("Error AgentBase: Deriving class has no override for AgentBase.", method_name, "() method. Implement ", method_name, "() in the derived class.", sep="")

	# --------- THESE METHODS SHOULD BE OVERRIDEN IN DERIVED CLASSES ------

	# Initializes the model of a decision making agent.
	# This model can be a neural network.
	# This creates an untrained model.
	# Use load(), save(), and train() after calling this method if necessary.
	def make_model(self) -> None:
		AgentBase.__print_error_message_1("make_model")

	# Loads trained/untrained model from a file. 
	# This file usually contains the weights and architecture of a neural network model after it has been trained.
	# `filename` - path to a file containing the model
	def load(self, filename) -> None:
		AgentBase.__print_error_message_1("load")

	# Saves trained/untrained model to a file.
	# This file can be loaded the next time the script is run. 
	# `filename` - path to a file which will store the model
	def save(self, filename) -> None:
		AgentBase.__print_error_message_1("save")	

	# This method will train/optimize a model with Reinforcement Learning.
	# This method will only train on one epoch and return. That way
	# The training progress can be demonstrated during training.
	# Some models don't need to be trained like agents.Random.
	# In those cases, this function will return immediately.
	def train(self) -> None:
		AgentBase.__print_error_message_1("train")

	# Model chooses an action based on the current state of the game.
	# `env` - gym-retro environment - stores current game state. Is not modified by this function.
	# returns: action is returned as an interface.GamePad object.
	# *See source.interface.game_pad.py
	def decide(self, obs, info) -> list:
		AgentBase.__print_error_message_1("decide")

	# Returns name of agent as a string
	def name(self) -> str:
		AgentBase.__print_error_message_1("name")
		return "AgentBase"

	def to_string(self) -> str:
		AgentBase.__print_error_message_1("to_string")
		return "AgentBase"
			