import os
import sys
import torch

script_dir = os.path.dirname(os.path.abspath(__file__))
project_dir = os.path.abspath(script_dir + "/../..")

sys.path.append(os.path.abspath(project_dir + '/source/agents'))
sys.path.append(os.path.abspath(project_dir + '/source/interface'))
sys.path.append(os.path.abspath(project_dir + '/source/learning'))

from actor_critic import *

class ActorCriticA(ActorCritic):

	# ----------------------- Nested Classes ----------------------------------

	class Actor(torch.nn.Module):
		def __init__(self):
			self.make_model()
		
		def make_model(self):
			super(ActorCriticA.Actor, self).__init__()

			# --- Skip Input Layer ---
			# TODO: define input layer explicitly
			# For now, input layer will be created on the 1st forward pass.

			# *** Shouldn't we use Conv3d for colored images? ***
			# *** No Conv2d is for both gray and colored images ***

			# --- Hidden Layer 1 ---
			self.conv1 = torch.nn.Sequential(
				torch.nn.Conv2d(
					in_channels=3,		# 3 channel color image
					out_channels=4,		# I think this means each convolutional kernel will output 16 nodes. See below ***
					kernel_size=5,		# Size of each kernel in both dimentions: 5x5. I think this should be the size of each object in the game.
					stride=1,			# Create a kernel on every so many pixels. 1 means don't skip any pixels.
					padding=2,			# ???
				),
				torch.nn.Sigmoid(),		# activation function. experiment with this. Try sigmoid.
				torch.nn.MaxPool2d(2),	# Pick the highest outputs of each group of 2x2 nodes.
			)

			# --- Hidden Layer 2 ---
			self.conv2 = torch.nn.Sequential(
				torch.nn.Conv2d(
					in_channels=4,		# TODO: This should equal to out_channels from the previous layer. Should not be hard coded.		
					out_channels=4,		# TODO: experiment with this
					kernel_size=5,		# TODO: experiment with this
					stride=1,			# 
					padding=2,			# 
				),
				torch.nn.Sigmoid(),		# 
				torch.nn.MaxPool2d(2),	# 
				)

			# --- Output Layers ---
			self.actor = torch.nn.Sequential(
				torch.nn.Linear(
					in_features=1200,
					out_features=ActionSpace.get_n_moves(),	# 1 output for each possible move. See class ActionSpace
				),
				torch.nn.Sigmoid(),							# Maybe use sigmoid to cap range to [0.0, 1.0]
			)

			# *** I think out_channels is the number of output nodes that each kernel outputs.
			# For example, out_channels=16 creates 16 output nodes for each kernel. 
			# In any NN, each node extracts a feature from its input.
			# out_channels says how many different features to extract from that kernel.
			# This is like saying 16 different features will be extracted from each kernel.
			# The more features, the more the NN can learn.
			# Should this number correspond to the the number of output classes???
			# Its worth testing.

			return

		def forward(self, x):
			x = x.reshape(x.size(dim=2), x.size(dim=0), x.size(dim=1))
			x = torch.unsqueeze(x, 0)
			x = x.float()
			x = self.conv1(x)
			x = self.conv2(x)
			x = x.view(x.size(0), -1)
			x = self.actor(x)		# Get action from actor layer
			# TODO: change this to max so that its not random
			distribution = Categorical(F.softmax(x, dim=1))	# TODO: Try dim=-1
			return distribution
		
	class Critic(torch.nn.Module):
		def __init__(self):
			self.make_model()
		
		def make_model(self):
			super(ActorCriticA.Critic, self).__init__()

			self.conv1 = torch.nn.Sequential(
				torch.nn.Conv2d(
					in_channels=3,		# 3 channel color image
					out_channels=4,		# I think this means each convolutional kernel will output 16 nodes. See below ***
					kernel_size=5,		# Size of each kernel in both dimentions: 5x5. I think this should be the size of each object in the game.
					stride=1,			# Create a kernel on every so many pixels. 1 means don't skip any pixels.
					padding=2,			# ???
				),
				torch.nn.Sigmoid(),		# activation function. experiment with this. Try sigmoid.
				torch.nn.MaxPool2d(2),	# Pick the highest outputs of each group of 2x2 nodes.
			)

			self.conv2 = torch.nn.Sequential(
				torch.nn.Conv2d(
					in_channels=4,		# TODO: This should equal to out_channels from the previous layer. Should not be hard coded.		
					out_channels=4,	# TODO: experiment with this
					kernel_size=5,		# TODO: experiment with this
					stride=1,			# 
					padding=2,			# 
				),
				torch.nn.Sigmoid(),		# 
				torch.nn.MaxPool2d(2),	# 
				)

			self.critic = torch.nn.Sequential(
				torch.nn.Linear(
					in_features=1200,
					out_features=1,	# 1 output for each possible move. See class ActionSpace
				),
				torch.nn.Sigmoid(),							# Maybe use sigmoid to cap range to [0.0, 1.0]
			)

		def forward(self, x):
			x = x.reshape(x.size(dim=2), x.size(dim=0), x.size(dim=1))
			x = torch.unsqueeze(x, 0)
			x = x.float()
			x = self.conv1(x)
			x = self.conv2(x)
			x = x.view(x.size(0), -1)
			x = self.critic(x)		# Get action from actor layer

			return x
	
	# --------------------------------- METHODS -------------------------------

	def __init__(self):
		self.make_model()
		
	def make_model(self):
		self.actor = ActorCriticA.Actor()
		self.critic = ActorCriticA.Critic()
		
		self.actor.make_model()
		self.critic.make_model()

	def forward(self, x):
		output = self.actor.forward(x)
		
		return output

	# Returns name of agent as a string
	def name(self) -> str:
			return "ActorCritic_A"
	