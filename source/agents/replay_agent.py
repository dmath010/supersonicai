# Various imports as needed with system paths set
import sys
import os
import retro

script_dir = os.path.dirname(os.path.abspath(__file__))
project_dir = os.path.abspath(script_dir + "/../..")

sys.path.append(os.path.abspath(project_dir + '/source/agents'))
sys.path.append(os.path.abspath(project_dir + '/source/interface'))

from agent_base import *

class ReplayAgent(AgentBase):
	def __init__(self, filename='SonicTheHedgehog-Genesis-GreenHillZone.Act1-0000.bk2'):
		# filename = level.game_name() + '-' + level.to_state() + '-0000.bk2'

		# We need to figure out how to get the proper stage from play.py
		stage = filename
		movie_path = f'{project_dir}/source/datasets/contest/' + stage
		self.movie = retro.Movie(movie_path)
		self.movie.step()
		self.moves = []
		while self.movie.step():
			keys = []
			for i in range(12):
				keys.append(self.movie.get_key(i, 0))
			self.moves.append(keys)
		self.i = -1

	def save(self, filename):
		None	# nothing to do here

	def train(self):
		None	# nothing to do here

	def decide(self, obs, info) -> list:
		self.i += 1
		return self.moves[self.i]

	# Returns name of agent as a string
	def name(self) -> str:
		return "ReplayAgent"

	def to_string(self) -> str:
		return self.name()