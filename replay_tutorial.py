# Various imports as needed with system paths set
import sys
import os
import retro
import time

sys.path.append(os.path.abspath('agents/'))
sys.path.append(os.path.abspath('interface/'))
from source.agents.agent_base import *

stage = 'SonicTheHedgehog-Genesis-ScrapBrainZone.Act2-0000.bk2'
movie_path = 'source/datasets/contest/' + stage
movie = retro.Movie(movie_path)
movie.step()

# Overlap with the play.py file, can probably merge here, level select
env = retro.make(game=movie.get_game(), state=retro.State.NONE, use_restricted_actions=retro.Actions.ALL)
env.initial_state = movie.get_state()
env.reset()

# Steps go under the decide method here - this is the one we want
print('stepping movie')
while movie.step():
    keys = []
    for i in range(len(env.buttons)):
        keys.append(movie.get_key(i, 0))
    _obs, _rew, _done, _info = env.step(keys)
    env.render()
    saved_state = env.em.get_state()
    time.sleep(0.005)

# Not really sure what this one is
# print('stepping environment started at final state of movie')
# env.initial_state = saved_state
# env.reset()
# while True:
# 	env.render()
# 	env.step(env.action_space.sample())